@extends('clients.layouts.clients-sidebar-left')


@section('page-title')
    {{ trans('pagetitles.hotels') }}
@endsection

@section('breadcrumbs')
    <li><a href="{{url('/')}}">{{ trans('pagetitles.home_br') }}</a></li>
    <li><a href="#" class="active">{{ trans('pagetitles.hotels_br') }}</a></li>
@endsection


@section('left-sidebar')
    <h4 class="search-results-title"><i class="soap-icon-search"></i><span id="num_found_results"></span></h4>
    <div class="toggle-container filters-container">
        {{ Form::open(['id' => 'hotelsFilterForm']) }}

        <div class="panel style1 arrow-right">
            <h4 class="panel-title">Филтри</h4>
            <div class="panel-content">
                    <div class="form-group">
                        <label>{{ trans('hotels.name') }}</label>
                        {{ Form::text('formData[main][name]', @$params['main']['name'], [ 'class' => 'input-text full-width' ] ) }}
                    </div>
                    <div class="form-group">
                        <label>{{ trans('hotels.type') }}</label>
                        {{ Form::select('formData[main][hotel_type][]', $hotel_type, @$params['main']['hotel_type'], ['id' => 'type', 'class'=>'search-select', 'multiple'=>'multiple']) }}
                    </div>
                    <div class="form-group">
                        <label>{{ trans('hotels.stars') }}</label>
                        {{ Form::select('formData[main][stars][]', $stars, @$params['main']['stars'], ['id' => 'stars', 'class'=>'search-select', 'multiple'=>'multiple']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::hidden('formData[rooms][price_min]', @$params['rooms']['price_min'], [ 'id' => 'price_min', 'class' => 'input-text full-width' ] ) }}
                        {{ Form::hidden('formData[rooms][price_max]', @$params['rooms']['price_max'], [ 'id' => 'price_max', 'class' => 'input-text full-width' ] ) }}
                        <label>{{ trans('hotels.price_range') }}</label>
                        <div class="price_range range-slider"></div>
                    </div>
            </div>
        </div>

        <div class="panel style1 arrow-right">
            <h4 class="panel-title">
                <a data-toggle="collapse" href="#facilities-filter" class="collapsed">Екстри</a>
            </h4>
            <div id="facilities-filter" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-content">
                    <div class="form-group">
                        <label>{{ trans('hotels.hotel_facilities') }}</label>
                        {{ Form::select('formData[hotel_facilities][]', $hotel_facilities, @$params['hotel_facilities'], ['id' => 'hotel_facilities', 'class'=>'multi-checkbox', 'multiple'=>'multiple']) }}
                    </div>
                    <div class="form-group">
                        <label>{{ trans('hotels.hotel_room_facilities') }}</label>
                        {{ Form::select('formData[hotel_room_facilities][]', $room_facilities, @$params['hotel_room_facilities'], ['id' => 'hotel_room_facilities', 'class'=>'multi-checkbox', 'multiple'=>'multiple']) }}
                    </div>
                </div><!-- end content -->
            </div>
        </div>

        <div class="panel style1 arrow-right">
            <h4 class="panel-title">
                <a data-toggle="collapse" href="#location-filter" class="collapsed">Местоположение</a>
            </h4>
            <div id="location-filter" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-content">
                    <div class="form-group">
                        <label>{{ trans('hotels.hotel_area') }}</label>
                        {{ Form::select('formData[main][area]', $locate_area, @$params['main']['area'], ['id' => 'locate_area', 'class'=>'search-select']) }}
                    </div>

                    <div class="form-group">
                        <label>{{ trans('hotels.hotel_municipalities') }}</label>
                        {{ Form::select('formData[main][municipality]', $locate_municipalities, @$params['main']['municipality'], ['id' => 'locate_municipality', 'class'=>'search-select']) }}
                    </div>
                </div><!-- end content -->
            </div>
        </div>

        <div class="panel style1 arrow-right">
            <h4 class="panel-title">
                <a data-toggle="collapse" href="#distance-filter" class="collapsed">Разтояния</a>
            </h4>
            <div id="distance-filter" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-content">
                    <div class="form-group">
                        <label>{{ trans('hotels.hotel_ski') }}</label>
                        {{ Form::hidden('formData[main][distance_ski]', @$params['main']['distance_ski'], [ 'id' => 'distance_ski', 'class' => 'input-text full-width' ] ) }}
                        <div class="slider_ski range-slider"></div>
                    </div>
                    <br>
                    <div class="form-group">
                        {{ Form::hidden('formData[main][distance_sea]', @$params['main']['distance_sea'], [ 'id' => 'distance_sea', 'class' => 'input-text full-width' ] ) }}
                        <label>{{ trans('hotels.hotel_beach') }}</label>

                        <div class="slider_beach range-slider"></div>
                    </div>
                </div><!-- end content -->
            </div>
        </div>
        {{ Form::hidden('method', 'Hotel.getSearchedHotels', [ 'id' => 'method' ] ) }}
        {{ Form::hidden('formData[page]', @$params['page'], [ 'id'=> 'page'] ) }}
        {{ Form::hidden('formData[ordering]', @$params['hotels_ordering'], [ 'id'=> 'hotels_ordering' ] ) }}
        <button id="filterSearchBtn" class="btn-medium icon-check uppercase full-width">{{ trans('hotels.search') }}</button>
        {{ Form::close() }}

    </div>
@endsection

@section('content')
    <div class="sort-by-section clearfix">
        <h4 class="sort-by-title block-sm">{{ trans('hotels.hotels_ordering') }}</h4>
        {{ Form::select('formData[hotels_ordering]', $hotels_ordering, @$params['hotels_ordering'], ['id' => 'hotels_ordering', 'class'=>'ordering-select']) }}

        <ul class="swap-tiles clearfix block-sm">
            <li class="swap-list">
                <a href="hotel-list-view.html"><i class="soap-icon-list"></i></a>
            </li>
            <li class="swap-grid active">
                <a href="hotel-grid-view.html"><i class="soap-icon-grid"></i></a>
            </li>
            <li class="swap-block">
                <a href="hotel-block-view.html"><i class="soap-icon-block"></i></a>
            </li>
        </ul>
    </div>
    <div class="hotel-list listing-style3 hotel">


    </div>
@endsection

@section('scripts')

    <script src="{{ URL::asset('/js/pages/hotels.js') }}"></script>

@endsection