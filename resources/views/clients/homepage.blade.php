@extends('clients.layouts.clients-full-width')


@section('page-title')
    {{ trans('pagetitles.home') }}
@endsection

@section('breadcrumbs')
    <li><a href="#" class="active">{{ trans('pagetitles.home_br') }}</a></li>
@endsection


@section('content')
    <div class="top-section">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <iframe width="100%" height="480px;" src="http://localhost/bv/public/assets/smartslider/sliders/4/index.html"></iframe>
                </div>
                <div class="col-md-4">
                    <h2>Препоръчани</h2>
                    <div class="hotel-list listing-style3 hotel">
                        @foreach($featuredHotels as $featuredHotel)
                            <article class="box">
                                <figure class="col-sm-5 col-md-4">
                                    <a title="" href="{{ url('hotels/'.$featuredHotel['main']['slug']) }}" class="hover-effect"><img width="270" height="160" alt="" src="{{ $featuredHotel['hotel_main_photo'] }}"></a>
                                </figure>
                                <div class="details col-sm-7 col-md-8">
                                    <div>
                                        <div>
                                            <a href="{{ url('hotels/'.$featuredHotel['main']['slug']) }}"><h4 class="box-title">{{ @$featuredHotel['main']['name'] }}</a><small><i class="soap-icon-departure yellow-color"></i> {{ @$featuredHotel['main']['municipality'] }}, {{ @$featuredHotel['main']['city'] }} </small></h4>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container section">

        <h2>Хотели</h2>
        <div class="row image-box style10">
            @foreach($randomHotels as $randomHotel)
                <div class="col-sms-6 col-sm-6 col-md-3">
                    <article class="box">
                        <figure class="animated fadeInDown box-image" data-animation-type="fadeInDown" data-animation-duration="1" style="animation-duration: 1s; visibility: visible;">
                            <a href="{{ url('hotels/'.$randomHotel['main']['slug']) }}" title="" class="hover-effect"><img src="{{ $randomHotel['hotel_main_photo'] }}" alt="" width="270" height="160"></a>
                        </figure>
                        <div class="details">
                            <a href="{{ url('hotels/'.$randomHotel['main']['slug']) }}">
                                <h4 class="box-title">{{ @$randomHotel['main']['name'] }}</h4>
                            </a>

                        </div>
                    </article>
                </div>
            @endforeach
        </div>
    </div>
    <div class="features section global-map-area parallax" data-stellar-background-ratio="0.5" style="background-position: 50% 73px;">
        <div class="text-center description titles-block">
            <h2 class="big-title">Лятото идва</h2>
            <span class="sub-title">Ето няколко наши предложения, къде да прекараш своята лятна ваканция.</span>
        </div>
        <div class="container">
            <div class="row image-box hotel listing-style1">
                @foreach($seaHotels as $seaHotel)
                    <div class="col-sms-6 col-sm-6 col-md-3">
                        <article class="box">
                            <figure class="animated fadeInDown" data-animation-type="fadeInDown" data-animation-delay="0" style="animation-duration: 1s; visibility: visible;">
                                <a class="hover-effect" href="{{ url('hotels/'.$seaHotel['main']['slug']) }}" title=""><img width="270" height="160" src="{{ $seaHotel['hotel_main_photo'] }}" alt=""></a>
                            </figure>
                            <div class="details">
                                    <span class="price">
                                        <small>Цени от</small>
                                        {{ @$seaHotel['main']['lowest_price'] }} {{ trans('global.currency_small') }}
                                    </span>
                                <h4 class="box-title"><a href="{{ url('hotels/'.$seaHotel['main']['slug']) }}">{{ @$seaHotel['main']['name'] }}</a><small>{{ @$seaHotel['main']['municipality'] }}, {{ @$seaHotel['main']['city'] }}</small></h4>
                                <div class="feedback">
                                    <div title="" class="five-stars-container" data-toggle="tooltip" data-placement="bottom" data-original-title="4 stars"><span class="five-stars" style="width: {{ $seaHotel['main']['ratings']['percent']['average'] }}%;"></span></div>
                                    <span class="review">{{$seaHotel['main']['ratings']['count']}} {{ ($seaHotel['main']['ratings']['count'] == 1 ? trans('hotels.review') : trans('hotels.reviews')) }}</span>
                                </div>
                                <p class="description">{!! \Illuminate\Support\Str::words($seaHotel['main']['description'], 20,'....') !!}</p>
                            </div>
                        </article>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="section container">
        <h2>Нощувки до 10 лв</h2>
        <div class="row image-box hotel listing-style1">
            @foreach($price10Hotels as $price10Hotel)
                <div class="col-sms-6 col-sm-6 col-md-3">
                    <article class="box">
                        <figure class="animated fadeInDown" data-animation-type="fadeInDown" data-animation-delay="0" style="animation-duration: 1s; visibility: visible;">
                            <a class="hover-effect" href="{{ url('hotels/'.$price10Hotel['main']['slug']) }}" title=""><img width="270" height="160" src="{{ $price10Hotel['hotel_main_photo'] }}" alt=""></a>
                        </figure>
                        <div class="details">
                                    <span class="price">
                                        <small>Цени от</small>
                                        {{ @$price10Hotel['main']['lowest_price'] }} {{ trans('global.currency_small') }}
                                    </span>
                            <h4 class="box-title"><a href="{{ url('hotels/'.$price10Hotel['main']['slug']) }}">{{ @$price10Hotel['main']['name'] }}</a><small>{{ @$price10Hotel['main']['municipality'] }}, {{ @$price10Hotel['main']['city'] }}</small></h4>
                            <div class="feedback">
                                <div title="" class="five-stars-container" data-toggle="tooltip" data-placement="bottom" data-original-title="4 stars"><span class="five-stars" style="width: {{ $price10Hotel['main']['ratings']['percent']['average'] }}%;"></span></div>
                                <span class="review">{{$price10Hotel['main']['ratings']['count']}} {{ ($price10Hotel['main']['ratings']['count'] == 1 ? trans('hotels.review') : trans('hotels.reviews')) }}</span>
                            </div>
                            <p class="description">{!! \Illuminate\Support\Str::words($price10Hotel['main']['description'], 20,'....') !!}</p>
                        </div>
                    </article>
                </div>
            @endforeach
        </div>
    </div>

    <div class="section container">
        <div class="col-md-4">
            <h2>Най-разглеждани тази седмица</h2>
            <div class="image-box hotel listing-style1">
                @foreach($topViewedWeek as $hotel)
                    <article class="box row">
                        <div class="col-md-4">
                            <figure class=""style="animation-duration: 1s; visibility: visible;">
                                <a class="" href="{{ url('hotels/'.$hotel['main']['slug']) }}" title=""><img width="270" height="160" src="{{ $hotel['hotel_main_photo'] }}" alt=""></a>
                            </figure>
                        </div>
                        <div class="col-md-8">
                            <div class="details">
                                        <span class="price">
                                            <small>Цени от</small>
                                            {{ @$hotel['main']['lowest_price'] }} {{ trans('global.currency_small') }}
                                        </span>
                                <h4 class="box-title"><a href="{{ url('hotels/'.$hotel['main']['slug']) }}">{{ @$hotel['main']['name'] }}</a><small>{{ @$hotel['main']['municipality'] }}, {{ @$hotel['main']['city'] }}</small></h4>

                            </div>
                        </div>
                    </article>
                @endforeach
            </div>
        </div>
        <div class="col-md-4">
            <h2>Най-търсени тази седмица</h2>
            <div class="image-box hotel listing-style1">
                @foreach($topSearchWeek as $hotel)
                    <article class="box row">
                        <div class="col-md-4">
                            <figure class=""style="animation-duration: 1s; visibility: visible;">
                                <a class="" href="{{ url('hotels/'.$hotel['main']['slug']) }}" title=""><img width="270" height="160" src="{{ $hotel['hotel_main_photo'] }}" alt=""></a>
                            </figure>
                        </div>
                        <div class="col-md-8">
                            <div class="details">
                                        <span class="price">
                                            <small>Цени от</small>
                                            {{ @$hotel['main']['lowest_price'] }} {{ trans('global.currency_small') }}
                                        </span>
                                <h4 class="box-title"><a href="{{ url('hotels/'.$hotel['main']['slug']) }}">{{ @$hotel['main']['name'] }}</a><small>{{ @$hotel['main']['municipality'] }}, {{ @$hotel['main']['city'] }}</small></h4>

                            </div>
                        </div>
                    </article>
                @endforeach
            </div>
        </div>
        <div class="col-md-4">
                <h2>Ревюта от obikolka.com</h2>
            <div class="block image-carousel style2 flexslider" data-animation="slide" data-item-width="360" data-item-margin="0">
                <ul class="slides image-box style1">
                    @foreach($reviews_posts as $review)
                        <li style="margin: 0">
                            <article class="box">
                                <figure>
                                    <a class="hover-effect" title="{{ $review['title'] }}" href="car-detailed.html">
                                        <img width="100%" alt="{{ $review['title'] }}" src="{{ $review['image'] }}"></a>
                                </figure>
                                <div class="reviews-details">
                                    <h4>
                                        <a href="{{ $review['link'] }}" title="{{ $review['title'] }}">
                                        {{ $review['title'] }}
                                        </a>
                                    </h4>
                                    <p>{{ $review['excerpt'] }}</p>
                                    <a class="button btn-small green" href="{{ $review['link'] }}" title="{{ $review['title'] }}">Прочети</a>
                                </div>
                            </article>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    <div class="container section">
        <h2>Интересни места</h2>
        <div class="row image-box style10">
            @foreach($destinations_posts as $dest_post)
                <div class="col-sms-6 col-sm-6 col-md-3">
                    <article class="box">
                        <figure class="animated fadeInDown" data-animation-type="fadeInDown" data-animation-duration="2" style="animation-duration: 2s; visibility: visible;">
                            <a href="{{ $dest_post['link'] }}" title="{{ $dest_post['title'] }}" class="hover-effect dest_image_link" style="background-image: url({{ $dest_post['image'] }});">
                            </a>
                        </figure>
                        <div class="details">
                                <h4 class="box-title">
                                    <a href="{{ $dest_post['link'] }}" title="{{ $dest_post['title'] }}">{{ $dest_post['title'] }}</a>
                                </h4>
                        </div>
                    </article>
                </div>
            @endforeach
        </div>
    </div>

@endsection

@section('scripts')


@endsection