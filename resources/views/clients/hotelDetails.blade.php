@extends('clients.layouts.clients-sidebar-right')


@section('page-title')
    {{--{{ trans('pagetitles.hotels') }}--}}
    {{ @$hotelDetails['main']['name'] }}
@endsection

@section('breadcrumbs')
    <li><a href="{{url('/')}}">{{ trans('pagetitles.home_br') }}</a></li>
    <li><a href="{{url('/hotels')}}">{{ trans('pagetitles.hotels_br') }}</a></li>
    <li><a href="#" class="active">{{@$hotelDetails['main']['name']}}</a></li>
@endsection


@section('right-sidebar')
    <div class="single-hotel-right">
        <div class="travelo-box contact-box">
            <h4>Интересни дестинации</h4>
            <ul class="ul-left-thumb">
                @foreach($destinations_posts as $dest)
                        <li>
                            <div class="thumb">
                                <a href="{{ $dest['link'] }}">
                                    <img src="{{ $dest['image'] }}" alt="" width="63" height="63">
                                </a>
                            </div>
                            <div class="description">
                                <h5 class="s-title"><a href="{{ $dest['link'] }}">{{ $dest['title'] }}</a></h5>
                                <p>{{ str_limit($dest['excerpt'], $limit = 80, $end = '...') }}</p>
                            </div>
                        </li>
                @endforeach
            </ul>
        </div>
        <div class="travelo-box contact-box">
            <h4>За връзка с obikolka.com</h4>
            <p>Ако имате нужда от помощ, имате съвет, предложение или критика. Не се колебайте да се свържете с нас.</p>
            <address class="contact-details">
                <span class="contact-phone"><i class="soap-icon-message"></i> info@obikolka.com</span>
            </address>
        </div>
        <div class="travelo-box">
            <h4>Хотели</h4>
            <div class="image-box style14">

                @foreach($randomHotels as $randomHotel)
                    <article class="box">
                        <figure>
                            <a href="{{ url('hotels/'.$randomHotel['main']['slug']) }}">
                                <img src="{{ $randomHotel['hotel_main_photo'] }}" alt="{{ @$randomHotel['main']['name'] }}" />
                            </a>
                        </figure>
                        <div class="details">
                            <h5 class="box-title"><a href="#">{{ @$randomHotel['main']['name'] }}</a></h5>
                            <label class="price-wrapper">
                                цени от <span class="price-per-unit">{{ @$randomHotel['main']['lowest_price'] }}лв.</span>
                            </label>
                        </div>
                    </article>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="single-hotel">
    <div class="tab-container style1 top-tabs" id="hotel-main-content">
        <ul class="tabs">
            <li class="active"><a data-toggle="tab" href="#photos-tab">{{ trans('hotel.gallery') }}</a></li>
            <li><a data-toggle="tab" href="#map-tab" id="googleMapTap">{{ trans('hotel.map') }}</a></li>
            <li><a data-toggle="tab" href="#contacts-tab">{{ trans('hotel.contacts') }}</a></li>
        </ul>
        <?php //echo '<pre><br>=====DEBUG START======<br> '.print_r($hotelDetails['hotel_gallery']['thumbnail'],1).' <br>=====DEBUG END======<br></pre>'; exit; ?>
        <div class="tab-content">
            <div id="photos-tab" class="tab-pane fade in active">
                <div class="photo-gallery style1" data-animation="slide" data-sync="#photos-tab .image-carousel">
                    <ul class="slides">
                        <li><img src="{{$hotelDetails['hotel_main_photo']}}" alt="" /></li>
                        @foreach($hotelDetails['hotel_gallery']['medium'] as $galleryImage)
                            <li><img src="{{$galleryImage['url']}}" alt="" /></li>
                        @endforeach
                    </ul>
                </div>
                <div class="image-carousel style1" data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#photos-tab .photo-gallery">
                    <ul class="slides">
                        @foreach($hotelDetails['hotel_gallery']['thumbnail'] as $galleryImage)
                            <li><img src="{{$galleryImage['url']}}" alt="" /></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div id="map-tab" class="tab-pane fade">
                <div id="hotelMap"></div>
                <input type="hidden" id="hotel_lat" value="{{ @$hotelDetails['main']['hotel_lat'] }}">
                <input type="hidden" id="hotel_lng" value="{{ @$hotelDetails['main']['hotel_lng'] }}">
            </div>
            <div id="contacts-tab" class="tab-pane fade" style="height: 500px;">
                    <div class="col-md-4 contact-info">
                        <h4>{{ trans('hotel.contact_information') }}</h4>
                        <div class="contact-group">
                            <div class="title">{{ trans('hotel.contact_name') }}</div>
                            <div class="contact-value">{{ $hotelDetails['main']['contact_name'] }}</div>
                        </div>
                        <div class="contact-group">
                        <div class="title">{{ trans('hotel.phone') }}</div>
                            <div class="contact-value">{{ $hotelDetails['main']['phone1'] }}
                            @if($hotelDetails['main']['phone2'] != '')
                                , {{ $hotelDetails['main']['phone1'] }}
                            @endif
                            </div>
                        </div>
                        @if($hotelDetails['main']['website'] != '')
                            <div class="contact-group">
                                <div class="title">{{ trans('hotel.website') }}</div>
                                <div class="contact-value">
                                    <a href="{{ $hotelDetails['main']['website'] }}" title="{{ $hotelDetails['main']['website'] }}" target="_blank">{{ $hotelDetails['main']['website'] }}</a>
                                </div>
                            </div>
                        @endif
                        <div class="contact-group">
                        <div class="title">{{ trans('hotel.area') }}</div>
                            <div class="contact-value">{{ $hotelDetails['main']['area'] }}</div>
                        </div>
                            <div class="contact-group">
                        <div class="title">{{ trans('hotel.municipality') }}</div>
                                <div class="contact-value">{{ $hotelDetails['main']['municipality'] }}</div>
                        </div>
                        <div class="contact-group">
                            <div class="title">{{ trans('hotel.city') }}</div>
                                <div class="contact-value">{{ $hotelDetails['main']['city'] }}</div>
                        </div>
                        @if($hotelDetails['main']['address'] != '')
                            <div class="contact-group">
                                <div class="title">{{ trans('hotel.address') }}</div>
                                <div class="contact-value">{{ $hotelDetails['main']['address'] }}</div>
                            </div>
                        @endif

                        @if($hotelDetails['main']['hotel_lat'] != '' && $hotelDetails['main']['hotel_lng'] )
                            <div class="contact-group">
                                <div class="title">{{ trans('hotel.coords') }}</div>
                                <div class="contact-value">{{ $hotelDetails['main']['hotel_lat'] }}, {{ $hotelDetails['main']['hotel_lng'] }} </div>
                            </div>
                        @endif

                    </div>
                    <div class="col-md-8 contact-form">
                        <h4>{{ trans('hotel.contact_us') }}</h4>
                        {{ Form::open(['name' => 'contactus', 'id' => 'contactus']) }}
                            <div class="form-group">
                                    <label>{{ trans('hotel.your_name') }}</label>
                                    {{ Form::text('formData[name]', '', [ 'data-field' => 'name', 'class' => 'input-text full-width'] ) }}
                            </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ trans('hotel.your_email') }}</label>
                                    {{ Form::text('formData[email]', '', [ 'data-field' => 'email', 'class' => 'input-text full-width'] ) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ trans('hotel.your_phone') }}</label>
                                    {{ Form::text('formData[phone]', '', [ 'data-field' => 'phone', 'class' => 'input-text full-width'] ) }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                                <label>{{ trans('hotel.your_message') }}</label>
                                {{ Form::textarea('formData[message]', '', [ 'data-field' => 'message', 'class' => 'input-text full-width'] ) }}
                        </div>
                        {{ Form::hidden('formData[hotel_id]', @$hotelDetails['main']['id'], [ 'id' => 'id', 'class' => 'hotel_id'] ) }}
                        {{ Form::hidden('method', 'Messages.sendToHotel', [ 'id' => 'method' ] ) }}
                        <button id="sendToHotelMsg">{{ trans('global.btn_send') }}</button>
                        {{ Form::close() }}
                    </div>
            </div>
        </div>
    </div>

    <div id="hotel-features" class="tab-container">
        <ul class="tabs">
            <li class="active"><a href="#hotel-description" data-toggle="tab">{{ trans('hotel.description') }}</a></li>
            <li><a href="#hotel-availability" data-toggle="tab">{{ trans('hotel.rooms') }}</a></li>
            <li><a href="#hotel-amenities" data-toggle="tab">{{ trans('hotel.facilities') }}</a></li>
            <li><a href="#hotel-reviews" data-toggle="tab">{{ trans('hotel.reviews') }}</a></li>
            {{--<li><a href="#hotel-faqs" data-toggle="tab">FAQs</a></li>--}}
            {{--<li><a href="#hotel-things-todo" data-toggle="tab">Things to Do</a></li>--}}
            {{--<li><a href="#hotel-write-review" data-toggle="tab">Write a Review</a></li>--}}
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="hotel-description">
                <div class="long-description">
                    <h1>{{ @$hotelDetails['main']['name'] }}</h1>
                    <p>
                        {{@$hotelDetails['main']['description']}}
                    </p>
                </div>
            </div>
            <div class="tab-pane fade" id="hotel-availability">
                <div class="flight-list listing-style3 flight">
                <?php //echo '<pre><br>=====DEBUG START======<br> '.print_r($hotelDetails['rooms'],1).' <br>=====DEBUG END======<br></pre>'; ?>
                @foreach($hotelDetails['rooms'] as $room)
                    <article class="box room">
                        <div class="details col-xs-9 col-sm-12">
                            <div class="details-wrapper">
                                <div class="first-row">
                                    <div>
                                        <h2 class="box-title">{{ $room['room_name'] }}</h2>
                                        <div class="room-description">
                                            {{ $room['room_description'] }}
                                        </div>
                                        <ul class="room-facilities">
                                            @foreach(@$room['facilities'] as $facility)
                                            <li><i class="fa fa-check" aria-hidden="true"></i>{{ $facility }}</li>
                                            @endforeach
                                        </ul>

                                    </div>
                                    <div>
                                        <span class="price"><small>{{ trans('hotel.room_price') }}</small>{{ $room['room_price'] }} {{ trans('global.currency_small') }}</span>
                                    </div>
                                </div>
                                <div class="second-row">
                                    <div class="time">
                                        <div class="take-off col-sm-4">
                                            <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                            <div>
                                                <span class="skin-color">{{ trans('hotel.room_type') }}</span><br>{{ $room['room_type'] }}
                                            </div>
                                        </div>
                                        <div class="landing col-sm-4">
                                            <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                            <div>
                                                <span class="skin-color">{{ trans('hotel.room_persons') }}</span><br>{{ $room['room_persons'] }}
                                            </div>
                                        </div>
                                        <div class="total-time col-sm-4">
                                            <div class="icon"><i class="soap-icon-clock yellow-color"></i></div>
                                            <div>
                                                <span class="skin-color">{{ trans('hotel.room_count') }}</span><br>{{ $room['room_count'] }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="action">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                @endforeach
                </div>
            </div>
            <div class="tab-pane fade" id="hotel-amenities">
                <h2>{{ trans('hotel.hotel_facilities_title') }}</h2>
                <p>{{ trans('hotel.hotel_facilities_description') }}</p>
                <ul class="hotel_facilities">
                    @foreach($hotelDetails['hotel_facilities'] as $hotel_facilitie)
                        <li><i class="fa fa-check" aria-hidden="true"></i>{{  $hotel_facilitie }}</li>
                    @endforeach
                </ul>
                <br />
                <h2>{{ trans('hotel.hotel_room_facilities_title') }}</h2>
                <p>{{ trans('hotel.hotel_room_facilities_description') }}</p>
                <ul class="hotel_facilities">
                    @foreach($hotelDetails['hotel_rooms_facilities'] as $hotel_room_facilitie)
                    <li><i class="fa fa-check" aria-hidden="true"></i>{{  $hotel_room_facilitie }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="tab-pane fade" id="hotel-reviews">
                <div class="intro table-wrapper full-width hidden-table-sms">
                    <div class="rating table-cell col-sm-4">
                        <span class="score"><?php echo $ratings['numbers']['average']; ?>/5.0</span>
                        <div class="five-stars-container"><div class="five-stars" style="width: <?php echo $ratings['percent']['average']; ?>%;"></div></div>
                        <a class="goto-writereview-pane button green btn-small full-width" id="write_review_btn">НАПИШЕТЕ РЕВЮ</a>
                    </div>
                    <div class="table-cell col-sm-8">
                        <div class="detailed-rating">
                            <ul class="clearfix">
                                <li class="col-md-6"><div class="each-rating">
                                        <label>Обслужване</label>
                                        <div class="five-stars-container">
                                            <div class="five-stars" style="width: <?php echo $ratings['percent']['service']; ?>%;"></div>
                                        </div>
                                    </div>
                                </li>
                                <li class="col-md-6">
                                    <div class="each-rating">
                                        <label>Храна</label>
                                        <div class="five-stars-container">
                                            <div class="five-stars" style="width: <?php echo $ratings['percent']['food']; ?>%;"></div>
                                        </div>
                                    </div>
                                </li>
                                <li class="col-md-6">
                                    <div class="each-rating">
                                        <label>Спокойствие</label>
                                        <div class="five-stars-container">
                                            <div class="five-stars" style="width: <?php echo $ratings['percent']['relax']; ?>%;"></div>
                                        </div>
                                    </div>
                                </li>
                                <li class="col-md-6">
                                    <div class="each-rating">
                                        <label>Чистота/хигиена</label>
                                        <div class="five-stars-container">
                                            <div class="five-stars" style="width: <?php echo $ratings['percent']['clean']; ?>%;"></div>
                                        </div>
                                    </div>
                                </li>
                                <li class="col-md-6">
                                    <div class="each-rating">
                                        <label>Местоположение</label>
                                        <div class="five-stars-container">
                                            <div class="five-stars" style="width: <?php echo $ratings['percent']['location']; ?>%;"></div>
                                        </div>
                                    </div>
                                </li>
                                <li class="col-md-6">
                                    <div class="each-rating">
                                        <label>Цена</label>
                                        <div class="five-stars-container">
                                            <div class="five-stars" style="width: <?php echo $ratings['percent']['price']; ?>%;"></div>
                                        </div>
                                    </div>
                                </li>
                                <div class="tip">*Минималната отценка е 1 звезда. При нито една засветната звезда означава, че хотелът все още не е отценен по този критерии.</div>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="hotel-write-review">
                    <h2>Оставете ревю</h2>
                    <h4>С цел избягване на фалшиви ревюта формата не е анонимна. Тя използва индификатора на Фейсбук и публикува ревюто с Вашето Фейсбук име. Моля пишете на кирилица и спазвайте добрия тон.</h4>
                    {{ Form::open(['name' => 'contactus', 'class' => 'review-form', 'id' => 'reviews_form']) }}
                        <div class="row">
                            <div class="form-group col-md-12 no-float no-padding">
                                <h4 class="title">Заглавие на ревюто</h4>
                                <input type="text" name="formData[review_title]"class="input-text full-width" value="" placeholder="Опишете с няколко думи Вашето мнение/преживяване (пр. 'Един незабравим семеен уикенд!')">
                            </div>
                        </div>
                    <div class="row">
                        <p>Моля отбележете категориите, само които искате да бъдат отценени. (пр. Ако в хотела не се предлага храна, Вие няма как да отцените храната, затова просто не давайте звезди за Храна)</p>

                        <div class="col-md-4">
                                <div class="service-rating">
                                    <h4>Обслужване</h4>
                                    <span class="rating-stars">

                                        <input type="radio" class="rating-input" id="rating-input-1-5" value="5" name="formData[service]">
                                        <label for="rating-input-1-5" class="rating-star"></label>

                                        <input type="radio" class="rating-input" id="rating-input-1-4" value="4" name="formData[service]">
                                        <label for="rating-input-1-4" class="rating-star"></label>

                                        <input type="radio" class="rating-input" id="rating-input-1-3" value="3" name="formData[service]">
                                        <label for="rating-input-1-3" class="rating-star"></label>

                                        <input type="radio" class="rating-input" id="rating-input-1-2" value="2" name="formData[service]">
                                        <label for="rating-input-1-2" class="rating-star"></label>

                                        <input type="radio" class="rating-input" id="rating-input-1-1" value="1" name="formData[service]">
                                        <label for="rating-input-1-1" class="rating-star"></label>

                                    </span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="food-rating">
                                    <h4>Храна</h4>
                                    <span class="rating-stars">

                                        <input type="radio" class="rating-input" id="rating-input-1-5-1" value="5" name="formData[food]">
                                        <label for="rating-input-1-5-1" class="rating-star"></label>

                                        <input type="radio" class="rating-input" id="rating-input-1-4-1" value="4" name="formData[food]">
                                        <label for="rating-input-1-4-1" class="rating-star"></label>

                                        <input type="radio" class="rating-input" id="rating-input-1-3-1" value="3" name="formData[food]">
                                        <label for="rating-input-1-3-1" class="rating-star"></label>

                                        <input type="radio" class="rating-input" id="rating-input-1-2-1" value="2" name="formData[food]">
                                        <label for="rating-input-1-2-1" class="rating-star"></label>

                                        <input type="radio" class="rating-input" id="rating-input-1-1-1" value="1" name="formData[food]">
                                        <label for="rating-input-1-1-1" class="rating-star"></label>

                                    </span>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="food-rating">
                                    <h4>Спокойствие</h4>
                                    <span class="rating-stars">

                                        <input type="radio" class="rating-input" id="rating-input-1-5-2" value="5" name="formData[relax]">
                                        <label for="rating-input-1-5-2" class="rating-star"></label>

                                        <input type="radio" class="rating-input" id="rating-input-1-4-2" value="4" name="formData[relax]">
                                        <label for="rating-input-1-4-2" class="rating-star"></label>

                                        <input type="radio" class="rating-input" id="rating-input-1-3-2" value="3" name="formData[relax]">
                                        <label for="rating-input-1-3-2" class="rating-star"></label>

                                        <input type="radio" class="rating-input" id="rating-input-1-2-2" value="1" name="formData[relax]">
                                        <label for="rating-input-1-2-2" class="rating-star"></label>

                                        <input type="radio" class="rating-input" id="rating-input-1-1-2" value="1" name="formData[relax]">
                                        <label for="rating-input-1-1-2" class="rating-star"></label>

                                    </span>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="service-rating">
                                    <h4>Чистота/Хигиена</h4>
                                    <span class="rating-stars">

                                            <input type="radio" class="rating-input" id="rating-input-1-5-3" value="5" name="formData[clean]">
                                            <label for="rating-input-1-5-3" class="rating-star"></label>

                                            <input type="radio" class="rating-input" id="rating-input-1-4-3" value="4" name="formData[clean]">
                                            <label for="rating-input-1-4-3" class="rating-star"></label>

                                            <input type="radio" class="rating-input" id="rating-input-1-3-3" value="3" name="formData[clean]">
                                            <label for="rating-input-1-3-3" class="rating-star"></label>

                                            <input type="radio" class="rating-input" id="rating-input-1-2-3" value="2" name="formData[clean]">
                                            <label for="rating-input-1-2-3" class="rating-star"></label>

                                            <input type="radio" class="rating-input" id="rating-input-1-1-3" value="1" name="formData[clean]">
                                            <label for="rating-input-1-1-3" class="rating-star"></label>

                                        </span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="food-rating">
                                    <h4>Местоположение</h4>
                                    <span class="rating-stars">

                                            <input type="radio" class="rating-input" id="rating-input-1-5-4" value="5" name="formData[location]">
                                            <label for="rating-input-1-5-4" class="rating-star"></label>

                                            <input type="radio" class="rating-input" id="rating-input-1-4-4" value="4" name="formData[location]">
                                            <label for="rating-input-1-4-4" class="rating-star"></label>

                                            <input type="radio" class="rating-input" id="rating-input-1-3-4" value="3" name="formData[location]">
                                            <label for="rating-input-1-3-4" class="rating-star"></label>

                                            <input type="radio" class="rating-input" id="rating-input-1-2-4" value="3" name="formData[location]">
                                            <label for="rating-input-1-2-4" class="rating-star"></label>

                                            <input type="radio" class="rating-input" id="rating-input-1-1-4" value="1" name="formData[location]">
                                            <label for="rating-input-1-1-4" class="rating-star"></label>

                                        </span>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="food-rating">
                                    <h4>Цена</h4>
                                    <span class="rating-stars">

                                            <input type="radio" class="rating-input" id="rating-input-1-5-5" value="5" name="formData[price]">
                                            <label for="rating-input-1-5-5" class="rating-star"></label>

                                            <input type="radio" class="rating-input" id="rating-input-1-4-5" value="4" name="formData[price]">
                                            <label for="rating-input-1-4-5" class="rating-star"></label>

                                            <input type="radio" class="rating-input" id="rating-input-1-3-5" value="3" name="formData[price]">
                                            <label for="rating-input-1-3-5" class="rating-star"></label>

                                            <input type="radio" class="rating-input" id="rating-input-1-2-5" value="2" name="formData[price]">
                                            <label for="rating-input-1-2-5" class="rating-star"></label>

                                            <input type="radio" class="rating-input" id="rating-input-1-1-5" value="1" name="formData[price]">
                                            <label for="rating-input-1-1-5" class="rating-star"></label>
                                        </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <h4 class="title">Как бихте описали хотела и престоя Ви в него</h4>

                            <textarea class="input-text full-width" name="formData[review_comment]" placeholder="enter your review (minimum 200 characters)" rows="5"></textarea>
                        </div>
                        <div class="form-group">
                            <h4 class="title">Какъв беше повода за посещението?</h4>
                            <ul class="sort-trip clearfix" id="visiting_types_btns">
                                @foreach($visiting_types as $key => $visiting_type)
                                <li data-type ="{{ $key }}"><a href="#">{!! $visiting_type['icon'] !!}</a><span>{{ $visiting_type['label'] }}</span></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="form-group col-md-5 no-float no-padding no-margin">
                            {{ Form::hidden('formData[hotel_id]', @$hotelDetails['main']['id'], [ 'id' => 'id', 'class' => 'hotel_id'] ) }}
                            <input type="hidden" id="visiting_type" value="" name="formData[visiting_type]">
                            <input type="hidden" name="method" value="Reviews.writeReview">
                            <div id="status"></div>

                            <button type="button" onlogin="_login();" id="sendReview" class="btn-large full-width">Изпрати</button>
                        </div>
                    {{ Form::close() }}
                </div>
                <div class="guest-reviews">
                    <h2>Ревюта за хотела</h2>
                    @foreach($reviews as $review)
                        <div class="guest-review table-wrapper">
                            <div class="col-xs-3 col-md-2 author table-cell">
                                <img src="https://graph.facebook.com/<?php echo $review->user_id; ?>/picture?type=normal" alt="" width="270" height="263" />
                                <p class="name">{{ $review->user_name }}</p>
                                <p class="date">{{ $review->created_at }}</p>
                            </div>
                            <div class="col-xs-9 col-md-10 table-cell comment-container">
                                <div class="comment-header clearfix">
                                    <h4 class="comment-title">{{ $review->review_title }}</h4>
                                    <div class="review-score">
                                        <div class="five-stars-container"><div class="five-stars" style="width: <?php echo $review->avgRatingPercents ?>%;"></div></div>
                                        <span class="score">{{ $review->avgRating }}/5.0</span>
                                    </div>
                                </div>
                                <div class="comment-content">
                                    <p>{{ $review->review_comment }}</p>
                                </div>
                                @if($review->visiting_type > 0)
                                    <div class="">Повод на посещението: <em>{{ $visiting_types[$review->visiting_type]['label'] }}</em></div>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ URL::asset('/template/components/revolution_slider/js/jquery.themepunch.plugins.min.js') }}"></script>
    <script src="{{ URL::asset('/template/components/revolution_slider/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ URL::asset('/template/components/jquery.bxslider/jquery.bxslider.min.js') }}"></script>
    <script src="{{ URL::asset('/template/components/flexslider/jquery.flexslider-min.js') }}"></script>
    <script src="{{ URL::asset('/assets/gmap/src/gmap3.js') }}"></script>
    <script src="{{ URL::asset('/js/pages/hotelDetails.js') }}"></script>
@endsection