<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 * Date: 12/17/16
 * Time: 4:44 PM
 */
?>

<nav id="main-menu" role="navigation">
    <ul class="menu">
        <li class="menu-item-has-children">
            <a href="{{url('/')}}">{{ trans('header.home') }}</a>
        </li>
        <li class="menu-item-has-children">
            <a href="{{url('/hotels')}}">{{ trans('header.hotels') }}</a>
        </li>
        <li class="menu-item-has-children">
            <a href="{{url('/offers')}}">{{ trans('header.offers') }}</a>
        </li>
        <li class="menu-item-has-children">
            <a href="{{url('/map')}}">{{ trans('header.map') }}</a>
        </li>
        <li class="menu-item-has-children">
            <a href="blog">{{ trans('header.blog') }}</a>
        </li>
    </ul>
</nav>
