<nav id="mobile-menu-01" class="mobile-menu collapse">
    <ul id="mobile-primary-menu" class="menu">
        <li class="menu-item-has-children">
            <a href="{{url('/')}}">{{ trans('header.home') }}</a>
        </li>
        <li class="menu-item-has-children">
            <a href="{{url('/hotels')}}">{{ trans('header.hotels') }}</a>
        </li>
        <li class="menu-item-has-children">
            <a href="{{url('/offers')}}">{{ trans('header.offers') }}</a>
        </li>
        <li class="menu-item-has-children">
            <a href="{{url('/map')}}">{{ trans('header.map') }}</a>
        </li>
    </ul>

    <ul class="mobile-topnav container">
        <li><a href="#">MY ACCOUNT</a></li>
        <li><a href="#travelo-login" class="soap-popupbox">LOGIN</a></li>
        <li><a href="#travelo-signup" class="soap-popupbox">SIGNUP</a></li>
    </ul>

</nav>