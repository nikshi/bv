<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 * Date: 12/17/16
 * Time: 4:45 PM
 */

?>

<ul class="tabs">
    <li class="{{ Request::is('dashboard') ? 'active' : '' }} dashboard-menu-btn"><a href="{{ url('') }}/dashboard"><i class="soap-icon-anchor circle"></i>{{ trans('dashboard.statistics') }}</a></li>
    <li class="{{ Request::is('dashboard/profile') ? 'active' : '' }} dashboard-menu-btn"><a href="{{ url('') }}/dashboard/profile"><i class="soap-icon-user circle"></i>{{ trans('dashboard.profile') }}</a></li>
    <li class="{{ Request::is('dashboard/hotel') ? 'active' : '' }} dashboard-menu-btn"><a href="{{ url('') }}/dashboard/hotel"><i class="soap-icon-businessbag circle"></i>{{ trans('dashboard.property') }}</a></li>
    <li class="{{ Request::is('dashboard/offers') ? 'active' : '' }} dashboard-menu-btn"><a href="{{ url('') }}/dashboard/offers"><i class="soap-icon-wishlist circle"></i>{{ trans('dashboard.offers') }}</a></li>
    <li class="{{ Request::is('dashboard/messages') ? 'active' : '' }} dashboard-menu-btn"><a href="{{ url('') }}/dashboard/messages"><i class="soap-icon-conference circle"></i>{{ trans('dashboard.messages') }}</a></li>
</ul>
