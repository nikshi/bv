<footer id="footer" class="style51">
    <div class="footer-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <h2>Настаняване по туризъм</h2>
                    <ul class="discover triangle hover row">
                        <li class="col-xs-6"><a href="#">Морски</a></li>
                        <li class="col-xs-6"><a href="#">Планински</a></li>
                        <li class="col-xs-6"><a href="#">Селски</a></li>
                        <li class="active col-xs-6"><a href="#">СПА</a></li>
                        <li class="col-xs-6"><a href="#">Активен</a></li>
                        <li class="col-xs-6"><a href="#">Приключенски</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <h2>Блог</h2>
                    <ul class="travel-news">
                        <li>
                            <div class="thumb">
                                <a href="#">
                                    <img src="https://placehold.it/63x63" alt="" width="63" height="63">
                                </a>
                            </div>
                            <div class="description">
                                <h5 class="s-title"><a href="#">Amazing Places</a></h5>
                                <p>Purus ac congue arcu cursus ut vitae pulvinar massaidp.</p>
                                <span class="date">25 Sep, 2013</span>
                            </div>
                        </li>
                        <li>
                            <div class="thumb">
                                <a href="#">
                                    <img src="https://placehold.it/63x63" alt="" width="63" height="63">
                                </a>
                            </div>
                            <div class="description">
                                <h5 class="s-title"><a href="#">Travel Insurance</a></h5>
                                <p>Purus ac congue arcu cursus ut vitae pulvinar massaidp.</p>
                                <span class="date">24 Sep, 2013</span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <h2>Промоции до 90%</h2>
                    <p>Запиши се в нашия бюлетин и получавай най-добрите оферти директно в твоята поща.</p>
                    <br>
                    <div class="icon-check">
                        <input type="text" class="input-text full-width" placeholder="Въведи твоя е-майл">
                    </div>
                    <br>
                    <a class="button btn-medium dull-blue">Запиши се</a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <h2>За нас</h2>
                    <p>Obikolka.com е туристически портал стремящ се да предоставя по най-удобен начин информация за нощувки, екскурзии, събития, атракции и друга информация свързана с Вашата обиколка</p>
                    <br>
                    <address class="contact-details">
                        <span class="contact-phone">За връзка с нас:</span>
                        <br>
                        <a href="#" class="contact-email">info@obikolka.com</a>
                    </address>
                    <ul class="social-icons clearfix">
                        <li class="twitter"><a title="" href="#" data-toggle="tooltip" data-original-title="twitter"><i class="soap-icon-twitter"></i></a></li>
                        <li class="googleplus"><a title="" href="#" data-toggle="tooltip" data-original-title="googleplus"><i class="soap-icon-googleplus"></i></a></li>
                        <li class="facebook"><a title="" href="#" data-toggle="tooltip" data-original-title="facebook"><i class="soap-icon-facebook"></i></a></li>
                        <li class="linkedin"><a title="" href="#" data-toggle="tooltip" data-original-title="linkedin"><i class="soap-icon-linkedin"></i></a></li>
                        <li class="vimeo"><a title="" href="#" data-toggle="tooltip" data-original-title="vimeo"><i class="soap-icon-vimeo"></i></a></li>
                        <li class="dribble"><a title="" href="#" data-toggle="tooltip" data-original-title="dribble"><i class="soap-icon-dribble"></i></a></li>
                        <li class="flickr"><a title="" href="#" data-toggle="tooltip" data-original-title="flickr"><i class="soap-icon-flickr"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom gray-area">
        <div class="container">
            <div class="logo pull-left">

            </div>
            <div class="pull-right">
                <a id="back-to-top" href="#" class="animated bounce" data-animation-type="bounce" style="animation-duration: 1s; visibility: visible;"><i class="soap-icon-longarrow-up circle"></i></a>
            </div>
            <div class="copyright pull-right">
                <p>© 2017 obikolka.com</p>
            </div>
        </div>
    </div>
</footer>