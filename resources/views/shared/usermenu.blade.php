@if( Auth::check() )

    <div class="topnav hidden-xs">
        <div class="container">
            <ul class="quick-menu pull-right">
                <li class="ribbon">
                    <a href="#">{{ Auth::user()->email }}</a>
                    <ul class="menu mini uppercase">
                        <li><a href="{{ url('dashboard/hotel') }}" class="location-reload">{{ trans('header.dashboard') }}</a></li>
                        <li>
                            <a href="{{ url('/logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ trans('header.logout') }}
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
@else

    <div class="topnav hidden-xs">
        <div class="container">
            <ul class="quick-menu pull-right">
                <li><a href={{url('/login')}}>Вход</a></li>
                <li><a href="{{url('/register')}}">Регистрация</a></li>
            </ul>
        </div>
    </div>

@endif
