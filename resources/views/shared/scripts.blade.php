<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 * Date: 12/17/16
 * Time: 5:18 PM
 */
?>
<script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDLbNYBE397VEiSsTdLFbLpcnn_b9LeRRQ"></script>


<!-- Javascript -->
<script src="https://code.jquery.com/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="
        crossorigin="anonymous"></script>

<script type="text/javascript" src="{{ URL::asset('/template/js/jquery-1.11.1.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/template/js/jquery.noconflict.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/template/js/modernizr.2.7.1.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/template/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/template/js/jquery-ui.1.10.4.min.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('/template/js/jquery.stellar.min.js') }}"></script>

<!-- Twitter Bootstrap -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>


<!-- load page Javascript -->
<script type="text/javascript" src="{{ URL::asset('/template/js/theme-scripts.js') }}"></script>

<!-- Plugins -->
{{--Loaders--}}
<link href="{{ URL::asset('/assets/loaders/fullscreen/css/HoldOn.min.css') }}" rel="stylesheet">
<script src="{{ URL::asset('/assets/loaders/fullscreen/js/HoldOn.min.js') }}"></script>

{{--Multiselect--}}
<script src="{{ URL::asset('/assets/multiSelect/jquery.multiselect.js') }}"></script>

{{--jQuery Range Slider--}}
<script src="{{ URL::asset('/assets/jQuery-ui-Slider-Pips-1.11.3/dist/jquery-ui-slider-pips.min.js') }}"></script>

{{--<link href="{{ URL::asset('/assets/loaders/btns/buttonLoader.css') }}" rel="stylesheet">--}}
{{--<script src="{{ URL::asset('/assets/loaders/btns/jquery.buttonLoader.min.js') }}"></script>--}}

{{--Notifications--}}
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<!-- Select with search -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<!-- load FlexSlider scripts -->
<script type="text/javascript" src="{{ URL::asset('/template/components/flexslider/jquery.flexslider-min.js') }}"></script>

<script src="{{ URL::asset('/js/consts.js') }}"></script>

<script src="{{ URL::asset('/js/ajax.js') }}"></script>
<script src="{{ URL::asset('/js/scripts.js') }}"></script>
