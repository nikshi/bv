<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 * Date: 12/17/16
 * Time: 5:18 PM
 */
?>

<!-- Plugins styles -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

<!--Files upload -->
<link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/dark-hive/jquery-ui.css" rel="stylesheet" />
<link href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/fileUpload/css/style.css') }}" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/fileUpload/css/jquery.fileupload.css') }}" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/fileUpload/css/jquery.fileupload-ui.css') }}" media="screen" />
<noscript><link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/fileUpload/css/jquery.fileupload-noscript.css') }}" media="screen" /></noscript>
<noscript><link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/fileUpload/css/jquery.fileupload-ui-noscript.css') }}" media="screen" /></noscript>


<!-- Multiselect Styles -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/multiSelect/jquery.multiselect.css') }}" />
{{--jQuery Range Slider--}}
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/jQuery-ui-Slider-Pips-1.11.3/dist/jquery-ui-slider-pips.min.css') }}" />


<!-- Theme Styles -->
<link rel="stylesheet" href="{{ URL::asset('/template/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('/template/css/font-awesome.min.css') }}">
<link href='https://fonts.googleapis.com/css?family=Lato:300,400,500,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="{{ URL::asset('/template/css/animate.min.css') }}">

<!-- Current Page Styles -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/template/components/revolution_slider/css/settings.css') }}" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/template/components/revolution_slider/css/style.css') }}" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/template/components/jquery.bxslider/jquery.bxslider.css') }}" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/template/components/flexslider/flexslider.css') }}" media="screen" />

<!-- Main Style -->
<link id="main-style" rel="stylesheet" href="{{ URL::asset('/template/css/style.css') }}">

<!-- Custom Styles -->
<link rel="stylesheet" href="{{ URL::asset('/css/custom.css') }}">


<!-- Responsive Styles -->
<link rel="stylesheet" href="{{ URL::asset('/template/css/responsive.css') }}">

<!-- CSS for IE -->
<!--[if lte IE 9]>
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/template/css/ie.css') }}" />
<![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script type='text/javascript' src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
<![endif]-->

