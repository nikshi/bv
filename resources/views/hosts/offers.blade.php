@extends('hosts.layouts.hosts')


@section('page-title')
    Табло за управление - Вашите оферти
@endsection

@section('breadcrumbs')
    <li><a href="{{url('/dashboard')}}">Табло за управление</a></li>
    <li><a href="#" class="active">Оферти</a></li>
@endsection


@section('content')
    <div id="offerts" class="tab-pane fade in active">
        Очаквайте скоро - Възможност за добавяне на оферта
    </div>
@endsection

@section('scripts')


@endsection