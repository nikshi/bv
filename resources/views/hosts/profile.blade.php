@extends('hosts.layouts.hosts')


@section('page-title')
    Табло за управление - Потребителски профил
@endsection

@section('breadcrumbs')
    <li><a href="{{url('/dashboard')}}">Табло за управление</a></li>
    <li><a href="#" class="active">Профил</a></li>
@endsection


@section('content')
    <div id="profile" class="tab-pane fade in active">
        <div class="view-profile">
            <h1>{{ trans('user.title') }} - {{ $user->email }}</h1>
            <h2>{{ trans('user.change_password') }}</h2>
            {{ Form::open(['name' => 'userForm', 'id' => 'userForm']) }}

            <div class="row form-group">
                <div class="col-sms-7 col-sm-7">
                    <label>{{ trans('user.old_password') }}</label>
                    {{ Form::password('formData[old_password]', [ 'id' => 'old_password', 'class' => 'input-text full-width'] ) }}
                </div>
            </div>

            <div class="row form-group">
                <div class="col-sms-7 col-sm-7">
                    <label>{{ trans('user.password') }}</label>
                    {{ Form::password('formData[password]', [ 'id' => 'password', 'class' => 'input-text full-width'] ) }}
                </div>
            </div>

            <div class="row form-group">
                <div class="col-sms-7 col-sm-7">
                    <label>{{ trans('user.password_confirmation') }}</label>
                    {{ Form::password('formData[password_confirmation]', [ 'id' => 'password_confirmation', 'class' => 'input-text full-width'] ) }}
                </div>
            </div>
            {{ Form::hidden('method', 'User.changePassword', [ 'id' => 'method' ] ) }}
            {{ Form::close() }}
            <button id="userSaveBtn">{{ trans('hotels.save') }}</button>
        </div>
    </div>
@endsection

@section('scripts')


@endsection