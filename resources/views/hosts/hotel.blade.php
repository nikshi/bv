@extends('hosts.layouts.hosts')


@section('page-title')
    Табло за управление - Редактиране на хотел
@endsection

@section('breadcrumbs')
    <li><a href="{{url('/dashboard')}}">Табло за управление</a></li>
    <li><a href="#" class="active">Редактиране на хотел</a></li>
@endsection


@section('content')

    <div id="objects" class="tab-pane fade in active">
            <div id="add_hotel" class="tab-container">
                <ul class="tabs">
                    <li class="active"><a href="#hotel_info" data-toggle="tab">{{ trans('hotels.main_info') }}</a></li>
                    <li><a href data-target="#hotel_gallery" data-toggle="tab">{{ trans('hotels.gallery') }}</a></li>
                    <li><a href data-target="#hotel_rooms" data-toggle="tab">{{ trans('hotels.rooms') }}</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="hotel_info">
                        {{ Form::open(['name' => 'hotelForm', 'id' => 'hotelForm']) }}
                            <button id="hotelSaveBtn">{{ trans('hotels.save') }}</button>

                            <div class="row form-group">
                                <div class="col-sms-7 col-sm-7">
                                    <label>{{ trans('hotels.hotel_name') }}</label>
                                    {{ Form::text('formData[mainInfo][name]', @$hotelDetails->name, [ 'id' => 'name', 'class' => 'input-text full-width'] ) }}
                                </div>
                                <div class="col-sms-5 col-sm-5 examples">
                                    {{ trans('hotels.hotel_name_description') }}
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-sms-7 col-sm-7">
                                    <label>{{ trans('hotels.hotel_stars') }}</label>
                                    {{ Form::select('formData[mainInfo][stars]', $stars, @$hotelDetails->stars,  [ 'id' => 'stars', 'class' => 'input-text full-width']) }}
                                </div>
                                <div class="col-sms-5 col-sm-5 examples">
                                    {{ trans('hotels.hotel_stars_description') }}
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-sms-7 col-sm-7">
                                    <label>{{ trans('hotels.hotel_type') }}</label>
                                    {{ Form::select('formData[mainInfo][hotel_type]', $hotel_type, @$hotelDetails->hotel_type, [ 'id' => 'hotel_type', 'class' => 'input-text full-width']) }}
                                </div>
                                <div class="col-sms-5 col-sm-5 examples">
                                    {{ trans('hotels.hotel_stars_description') }}
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-sms-12 col-sm-12">
                                    <label>{{ trans('hotels.hotel_description') }}</label>
                                    {{ Form::textarea('formData[mainInfo][description]', @$hotelDetails->description, [ 'id' => 'description', 'class' => 'input-text full-width'] ) }}
                                </div>
                            </div>

                            <h2>{{ trans('hotels.hotel_contacts') }}</h2>
                            <div class="row form-group">
                                <div class="col-sms-7 col-sm-7">
                                    <label>{{ trans('hotels.hotel_contact_name') }}</label>
                                    {{ Form::text('formData[mainInfo][contact_name]', @$hotelDetails->contact_name, [ 'id' => 'contact_name', 'class' => 'input-text full-width'] ) }}
                                </div>
                                <div class="col-sms-5 col-sm-5 examples">
                                    {{ trans('hotels.hotel_contact_name_description') }}
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-sms-7 col-sm-7">
                                    <label>{{ trans('hotels.hotel_phone1') }}</label>
                                    {{ Form::text('formData[mainInfo][phone1]', @$hotelDetails->phone1, [ 'id' => 'phone1', 'class' => 'input-text full-width'] ) }}
                                </div>
                                <div class="col-sms-5 col-sm-5 examples">
                                    {{ trans('hotels.hotel_phone1_description') }}
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-sms-7 col-sm-7">
                                    <label>{{ trans('hotels.hotel_phone2') }}</label>
                                    {{ Form::text('formData[mainInfo][phone2]', @$hotelDetails->phone2, [ 'id' => 'phone2', 'class' => 'input-text full-width'] ) }}
                                </div>
                                <div class="col-sms-5 col-sm-5 examples">
                                    {{ trans('hotels.hotel_stars_description') }}
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-sms-7 col-sm-7">
                                    <label>{{ trans('hotels.hotel_email') }}</label>
                                    {{ Form::text('formData[mainInfo][email]', @$hotelDetails->email, [ 'id' => 'email', 'class' => 'input-text full-width'] ) }}
                                </div>
                                <div class="col-sms-5 col-sm-5 examples">
                                    {{ trans('hotels.hotel_email_description') }}
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-sms-7 col-sm-7">
                                    <label>{{ trans('hotels.hotel_website') }}</label>
                                    {{ Form::text('formData[mainInfo][website]', @$hotelDetails->website, [ 'id' => 'website', 'class' => 'input-text full-width' ] ) }}
                                </div>
                                <div class="col-sms-5 col-sm-5 examples">
                                    {{ trans('hotels.hotel_stars_description') }}
                                </div>
                            </div>

                            <h2>{{ trans('hotels.hotel_location') }}</h2>

                            <div class="row form-group">
                                <div class="col-sms-7 col-sm-7 locate_area">
                                    <label>{{ trans('hotels.hotel_area') }}</label>
                                    {{ Form::select('formData[mainInfo][area]', $locate_area, @$hotelDetails->area, ['id' => 'locate_area', 'class'=>'search-select']) }}
                                </div>
                                <div class="col-sms-5 col-sm-5 examples">
                                    {{ trans('hotels.hotel_area_description') }}
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-sms-7 col-sm-7 locate_municipalities">
                                    <label>{{ trans('hotels.hotel_municipalities') }}</label>
                                    {{ Form::select('formData[mainInfo][municipality]', $locate_municipalities, @$hotelDetails->municipality, ['id' => 'locate_municipalities', 'class'=>'search-select' ]) }}
                                </div>
                                <div class="col-sms-5 col-sm-5 examples">
                                    {{ trans('hotels.hotel_municipality_description') }}
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-sms-7 col-sm-7">
                                    <label>{{ trans('hotels.hotel_city') }}</label>
                                    {{ Form::text('formData[mainInfo][city]', @$hotelDetails->city, [ 'id' => 'city', 'class' => 'input-text full-width' ] ) }}
                                </div>
                                <div class="col-sms-5 col-sm-5 examples">
                                    {{ trans('hotels.hotel_city_description') }}
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-sms-7 col-sm-7">
                                    <label>{{ trans('hotels.hotel_address') }}</label>
                                    {{ Form::text('formData[mainInfo][address]', @$hotelDetails->address, [ 'id' => 'address', 'class' => 'input-text full-width'] ) }}
                                </div>
                                <div class="col-sms-5 col-sm-5 examples">
                                    {{ trans('hotels.hotel_address_description') }}
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-sms-7 col-sm-7">
                                    <label>{{ trans('hotels.hotel_beach_dist') }}</label>
                                    {{ Form::text('formData[mainInfo][distance_sea]', @$hotelDetails->distance_sea, [ 'id' => 'distance_sea', 'class' => 'input-text full-width'] ) }}
                                </div>
                                <div class="col-sms-5 col-sm-5 examples">
                                    {{ trans('hotels.hotel_beach_dist_description') }}
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sms-7 col-sm-7">
                                    <label>{{ trans('hotels.hotel_beach_skii') }}</label>
                                    {{ Form::text('formData[mainInfo][distance_ski]', @$hotelDetails->distance_ski, [ 'id' => 'distance_ski', 'class' => 'input-text full-width' ] ) }}
                                </div>
                                <div class="col-sms-5 col-sm-5 examples">
                                    {{ trans('hotels.hotel_beach_skii_description') }}
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sms-12 col-sm-12">
                                    {{ trans('hotels.hotel_map_description') }}
                                    <button class="yellow" id="showMeOnMap">{{ trans('hotels.hotel_btn_map_locate') }}</button>
                                    <div id="dashboardHotelMap"></div>
                                </div>
                                {{ Form::hidden('formData[mainInfo][hotel_lat]', @$hotelDetails->hotel_lat, [ 'id' => 'hotel_lat' ] ) }}
                                {{ Form::hidden('formData[mainInfo][hotel_lng]', @$hotelDetails->hotel_lng, [ 'id' => 'hotel_lng'] ) }}
                            </div>

                            <div class="extras_heading">
                                <h2>{{ trans('hotels.hotel_extras') }}</h2>
                                <p>{{ trans('hotels.hotel_extras') }}</p>
                            </div>
                            <div class="extras_list">
                                @foreach ($hotel_facilities as $facility_id => $facility)
                                    <input type="hidden" name="formData[facilities][{{ $facility_id }}]" value="0">
                                    <label class="hotel_facility"><input type="checkbox" value="1"  name="formData[facilities][{{ $facility_id }}]" <?php if( $hotelFacilities ) echo (in_array($facility_id, $hotelFacilities)? 'checked':''); ?>> {{ $facility }}</label>
                                @endforeach
                            </div>
                        <div class="clearfix"></div>
                            {{ Form::hidden('formData[id]', @$hotelDetails->id, [ 'id' => 'id', 'class' => 'hotel_id'] ) }}
                            {{ Form::hidden('method', 'Hotel.save', [ 'id' => 'method' ] ) }}
                            <button id="hotelSaveBtn">{{ trans('hotels.save') }}</button>
                        {{ Form::close() }}
                    </div>

                    <div class="tab-pane fade" id="hotel_gallery">
                        @if( @$hotelDetails->id == 0 )
                            <div class="no-hotel">
                                <div class="col-md-1"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></div>
                                <div class="col-md-11"><h2>{{ trans('hotels.gallery_disable') }}</h2></div>
                            </div>
                        @endif
                        <div class="gallery-wrap @if( @$hotelDetails->id == 0 ) hidden @endif">
                            <div class="row">
                                <div class="col-md-6">
                                    <button id="uploadMainPhotoBtn">{{ trans('hotels.gallery_upload') }}</button>

                                    {{ Form::open(['name' => 'deleteMainPhoto', 'id' => 'deleteMainPhoto']) }}
                                        <button class="red" id="deleteMainPhotoBtn">{{ trans('hotels.gallery_delete') }}</button>
                                        {{ Form::hidden('formData[hotel_id]', @$hotelDetails->id, [ 'class' => 'hotel_id'] ) }}
                                        {{ Form::hidden('method', 'Files.delMainPhoto', [ 'id' => 'method' ] ) }}
                                    {{ Form::close() }}

                                    <div id="image-preview" style="background: url('{{@$hotel_main_photo}}') center center / cover">
                                        {{ Form::open(['name' => 'uploadMainPhoto', 'id' => 'uploadMainPhoto']) }}
                                            <label for="image-upload" id="image-label">{{ trans('hotels.gallery_add_main') }}</label>
                                            <input type="file" name="formData[mainPhoto]" id="image-upload" />
                                            {{ Form::hidden('formData[hotel_id]', @$hotelDetails->id, [ 'class' => 'hotel_id'] ) }}
                                            {{ Form::hidden('method', 'Files.uploadMainPhoto', [ 'id' => 'method' ] ) }}
                                        {{ Form::close() }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h2>{{ trans('hotels.gallery_main_title') }}</h2>
                                    <p>{{ trans('hotels.gallery_main_description') }}</p>
                                </div>
                            </div>

                            <h2>{{ trans('hotels.gallery_title') }}</h2>
                            <p>{{ trans('hotels.gallery_description') }}</p>


                            <!-- The file upload form used as target for the file upload widget -->
                            <form id="fileupload"  method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <!-- Redirect browsers with JavaScript disabled to the origin page -->
                                <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
                                <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                                <div class="row fileupload-buttonbar">
                                    <div class="col-lg-7">
                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                        <span class="btn btn-success fileinput-button">
                                                <i class="glyphicon glyphicon-plus"></i>
                                                <span>{{ trans('hotels.gallery_add') }}</span>
                                                <input type="file" name="files[]" multiple>
                                            </span>
                                        <button type="submit" class="btn btn-primary start">
                                            <i class="glyphicon glyphicon-upload"></i>
                                            <span>{{ trans('hotels.gallery_upload') }}</span>
                                        </button>
                                        <button type="reset" class="btn btn-warning cancel">
                                            <i class="glyphicon glyphicon-ban-circle"></i>
                                            <span>{{ trans('hotels.gallery_cancel') }}</span>
                                        </button>
                                        <button type="button" class="btn btn-danger delete">
                                            <i class="glyphicon glyphicon-trash"></i>
                                            <span>{{ trans('hotels.gallery_delete') }}</span>
                                        </button>
                                        <input type="checkbox" class="toggle">
                                        <!-- The global file processing state -->
                                        <span class="fileupload-process"></span>
                                    </div>
                                    <!-- The global progress state -->
                                    <div class="col-lg-5 fileupload-progress fade">
                                        <!-- The global progress bar -->
                                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                            <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                        </div>
                                        <!-- The extended global progress state -->
                                        <div class="progress-extended">&nbsp;</div>
                                    </div>
                                </div>
                                <!-- The table listing the files available for upload/download -->
                                <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
                                    {{ Form::hidden('formData[hotel_id]', @$hotelDetails->id, ['class' => 'hotel_id'])}}
                                <input type="hidden" name="method" value="Files.uploadGallery">
                            </form>
                            <br>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{ trans('hotels.gallery_notes') }}</h3>
                                </div>
                                <div class="panel-body">
                                   {{--TODO: notes--}}
                                </div>
                            </div>
                        </div>
                        <!-- The blueimp Gallery widget -->
                        <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
                            <div class="slides"></div>
                            <h3 class="title"></h3>
                            <a class="prev">‹</a>
                            <a class="next">›</a>
                            <a class="close">×</a>
                            <a class="play-pause"></a>
                            <ol class="indicator"></ol>
                        </div>
                        <!-- The template to display files available for upload -->
                        <script id="template-upload" type="text/x-tmpl">
                                {% for (var i=0, file; file=o.files[i]; i++) { %}
                                    <tr class="template-upload fade">
                                        <td>
                                            <span class="preview"></span>
                                        </td>
                                        <td>
                                            <p class="name">{%=file.name%}</p>
                                            <strong class="error text-danger"></strong>
                                        </td>
                                        <td>
                                            <p class="size">{{ trans('hotels.gallery_processing') }}</p>
                                            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
                                        </td>
                                        <td>
                                            {% if (!i && !o.options.autoUpload) { %}
                                                <button class="btn btn-primary start" disabled>
                                                    <i class="glyphicon glyphicon-upload"></i>
                                                    <span>{{ trans('hotels.gallery_upload') }}</span>
                                                </button>
                                            {% } %}
                                            {% if (!i) { %}
                                                <button class="btn btn-warning cancel">
                                                    <i class="glyphicon glyphicon-ban-circle"></i>
                                                    <span>{{ trans('hotels.gallery_cancel') }}</span>
                                                </button>
                                            {% } %}
                                        </td>
                                    </tr>
                                {% } %}
                            </script>
                        <!-- The template to display files available for download -->
                        <script id="template-download" type="text/x-tmpl">
                            {% for (var i=0, file; file=o.files[i]; i++) { %}
                                <tr class="template-download fade">
                                    <td>
                                        <span class="preview">
                                            {% if (file.thumbnailUrl) { %}
                                                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                                            {% } %}
                                        </span>
                                    </td>
                                    <td>
                                        <p class="name">
                                            {% if (file.url) { %}
                                                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                                            {% } else { %}
                                                <span>{%=file.name%}</span>
                                            {% } %}
                                        </p>
                                        {% if (file.error) { %}
                                            <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                                        {% } %}
                                    </td>
                                    <td>
                                        <span class="size">{%=o.formatFileSize(file.size)%}</span>
                                    </td>
                                    <td>
                                        {% if (file.deleteUrl) { %}
                                            <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                                                <i class="glyphicon glyphicon-trash"></i>
                                                <span>{{ trans('hotels.gallery_delete') }}</span>
                                            </button>
                                            <input type="checkbox" name="delete" value="1" class="toggle">
                                        {% } else { %}
                                            <button class="btn btn-warning cancel">
                                                <i class="glyphicon glyphicon-ban-circle"></i>
                                                <span>{{ trans('hotels.gallery_cancel') }}</span>
                                            </button>
                                        {% } %}
                                    </td>
                                </tr>
                            {% } %}
                            </script>
                    </div>


                    <div class="tab-pane fade" id="hotel_rooms">

                        @if( @$hotelDetails->id == 0 )
                            <div class="no-hotel">
                                <div class="col-md-1"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></div>
                                <div class="col-md-11"><h2>{{ trans('hotels.gallery_disable') }}</h2></div>
                            </div>
                        @endif
                        <div class="room-wrap @if( @$hotelDetails->id == 0 ) hidden @endif">
                            <h2>{{ trans('hotels.rooms_title') }}</h2>

                            {{ Form::open(['name' => 'hotelRooms', 'id' => 'hotelRooms']) }}

                                <button type="button" id="roomSaveBtn">{{ trans('hotels.save') }}</button>

                                <div class="row form-group">
                                    <div class="col-sms-7 col-sm-7">
                                        <label>{{ trans('hotels.room_type') }}</label>
                                        {{ Form::select('formData[roomInfo][room_type]', $room_types, 1,  [ 'data-field' => 'room_type', 'class' => 'input-text full-width']) }}
                                    </div>
                                    <div class="col-sms-5 col-sm-5 examples">
                                        {{ trans('hotels.room_type') }}
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-sms-7 col-sm-7">
                                        <label>{{ trans('hotels.room_name') }}</label>
                                        {{ Form::text('formData[roomInfo][room_name]', '', [ 'data-field' => 'room_name', 'class' => 'input-text full-width'] ) }}
                                    </div>
                                    <div class="col-sms-5 col-sm-5 examples">
                                        {{ trans('hotels.room_name_description') }}
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-sms-7 col-sm-7">
                                        <label>{{ trans('hotels.room_persons') }}</label>
                                        {{ Form::text('formData[roomInfo][room_persons]', '', [ 'data-field' => 'room_persons', 'class' => 'input-text full-width'] ) }}
                                    </div>
                                    <div class="col-sms-5 col-sm-5 examples">
                                        {{ trans('hotels.room_persons_description') }}
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-sms-7 col-sm-7">
                                        <label>{{ trans('hotels.room_count') }}</label>
                                        {{ Form::text('formData[roomInfo][room_count]', '', [ 'data-field' => 'room_count', 'class' => 'input-text full-width'] ) }}
                                    </div>
                                    <div class="col-sms-5 col-sm-5 examples">
                                        {{ trans('hotels.room_count_description') }}
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-sms-7 col-sm-7">
                                        <label>{{ trans('hotels.room_price') }}</label>
                                        {{ Form::text('formData[roomInfo][room_price]', '', [ 'data-field' => 'room_price', 'class' => 'input-text full-width'] ) }}
                                    </div>
                                    <div class="col-sms-5 col-sm-5 examples">
                                        {{ trans('hotels.room_price_description') }}
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-sms-12 col-sm-12">
                                        <label>{{ trans('hotels.room_description') }}</label>
                                        {{ Form::textarea('formData[roomInfo][room_description]', '', [ 'data-field' => 'room_description', 'class' => 'input-text full-width'] ) }}
                                    </div>
                                </div>

                                <div class="room_extras_list">
                                    @foreach ($room_facilities as $facility_id => $facility)
                                        <input type="hidden" data-facility="1" name="formData[facilities][{{ $facility_id }}]" value="0">
                                        <label class="room_facility"><input type="checkbox" value="1" data-field="facilities"  name="formData[facilities][{{ $facility_id }}]"> {{ $facility }}</label>
                                    @endforeach
                                </div>

                                {{ Form::hidden('formData[hotel_id]', @$hotelDetails->id, [ 'class' => 'hotel_id'] ) }}
                                {{ Form::hidden('formData[room_id]', 0, [ 'data-field' => 'room_id'] ) }}
                                {{ Form::hidden('method', 'Rooms.save', [ 'id' => 'method' ] ) }}

                            {{ Form::close() }}
                            <div class="clearfix"></div>
                            <h2>{{ trans('hotels.room_list_title') }}</h2>
                            <div class="rooms-list">

                            </div>
                        </div>
                    </div>

                </div>

            </div>
    </div>

@endsection

@section('scripts')

<!-- Files upload -->
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="{{ URL::asset('/assets/fileUpload/js/vendor/jquery.ui.widget.js') }}"></script>
<script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<script src="{{ URL::asset('/assets/fileUpload/js/jquery.iframe-transport.js') }}"></script>
<script src="{{ URL::asset('/assets/fileUpload/js/jquery.fileupload.js') }}"></script>
<script src="{{ URL::asset('/assets/fileUpload/js/jquery.fileupload-process.js') }}"></script>
<script src="{{ URL::asset('/assets/fileUpload/js/jquery.fileupload-image.js') }}"></script>
<script src="{{ URL::asset('/assets/fileUpload/js/jquery.fileupload-validate.js') }}"></script>
<script src="{{ URL::asset('/assets/fileUpload/js/jquery.fileupload-ui.js') }}"></script>
<script src="{{ URL::asset('/assets/fileUpload/js/main.js') }}"></script>

<script src="{{ URL::asset('/assets/fileUpload/jquery.uploadPreview.js') }}"></script>

<script src="{{ URL::asset('/js/pages/hotel.js') }}"></script>

@endsection