<!DOCTYPE html>
<html lang="en">
<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page Title -->
    <title>Административен пател за хотели - {{ config('app.name', 'Laravel') }}</title>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo | Responsive HTML5 Travel Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('shared.styles')

</head>
<body>

<header id="header" class="navbar-static-top">
    @include('shared.usermenu')

    <div class="main-header">

        <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">
            Mobile Menu Toggle
        </a>

        <div class="container">
            <h1 class="logo navbar-brand">
                <a href="{{url('/')}}" title="aaa">
                    <img src="{{ asset('template/images/logo.png') }}" alt="Logo" />
                </a>
            </h1>
            @include('shared.topmenu')
        </div>
        @include('shared.mobilemenu')
    </div>
</header>

<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">@yield('page-title')</h2>
        </div>
        <ul class="breadcrumbs pull-right">
            @yield('breadcrumbs')
        </ul>
    </div>
</div>

<section id="content" class="gray-area">
    <div class="container">
        <div id="main">
            <div class="tab-container full-width-style arrow-left dashboard">
                @include('shared.dashboard-menu')
                <div class="tab-content">

                    @yield('content')

                </div>
            </div>
        </div>
    </div>
</section>

@include('shared.footer')

@include('shared.scripts')

@yield('scripts')

</body>
</html>
