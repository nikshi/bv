@extends('hosts.layouts.hosts')


@section('page-title')
    Табло за управление - Съобщения
@endsection

@section('breadcrumbs')
    <li><a href="{{url('/dashboard')}}">Табло за управление</a></li>
    <li><a href="#" class="active">Съобщения</a></li>
@endsection



@section('content')
    <div id="messages" class="tab-pane fade in active">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>№</th>
                    <th>{{ trans('hotels.hotel_messages_from') }}</th>
                    <th>{{ trans('hotels.hotel_messages_date') }}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach($messages as $id => $message)
                    <tr class="@if($message['status'] == 0)unreaded-message @endif">
                        <th scope="row">{{ $message['id'] }}</th>
                        <td><a href="{{url('/dashboard/messages/'.$message['token'])}}">{{ $message['name'] }}</a></td>
                        <td>{{ $message['created_at'] }}</td>
                        <td><a href="#">{{ trans('hotels.hotel_messages_delete') }}</a></td>
                    </tr>
            @endforeach
            </tbody>
        </table>
        <?php //echo '<pre><br>=====DEBUG START======<br> '.print_r($messages,1).' <br>=====DEBUG END======<br></pre>'; ?>
    </div>
@endsection

@section('scripts')


@endsection