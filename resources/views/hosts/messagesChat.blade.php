@extends('hosts.layouts.hosts')


@section('page-title')
    Табло за управление - Съобщения
@endsection

@section('breadcrumbs')
    <li><a href="{{url('/dashboard')}}">Табло за управление</a></li>
    <li><a href="#" class="active">Съобщения</a></li>
@endsection



@section('content')
    <div id="messages" class="tab-pane fade in active">

    @foreach($messages as $message)

        @if($message['email'] == $user->email)
            <div class="chat-message message-host">
                <div class="info">
                    <div class="time-date">{{ @$message['created_at'] }}</div>
                    <div class="name-mail"><span class="name">{{ @$message['name']}}</span></div>
                </div>
                <hr>
                <div class="message">
                    {{ @$message['message'] }}
                </div>
            </div>
        @else
            <div class="chat-message message-client">
                <div class="info">
                    <div class="time-date">{{ @$message['created_at'] }}</div>
                    <div class="name-mail"><span class="name">{{ @$message['name']  }}</span> <span class="mail">({{ @$message['phone'] }})</span></div>
                </div>
                <hr>
                <div class="message">
                    {{ @$message['message'] }}
                </div>
            </div>
            <div class="clearfix"></div>
        @endif

    @endforeach
<div class="clearfix"></div>
        {{ Form::open(['name' => 'msgreply', 'id' => 'msgreply']) }}
        <div class="form-group">
            <label>{{ trans('hotel.reply') }}</label>
            {{ Form::textarea('formData[message]', '', [ 'data-field' => 'message', 'class' => 'input-text full-width'] ) }}
        </div>
        {{ Form::hidden('formData[hotel_id]', @$message['hotel_id'], [ 'id' => 'id', 'class' => 'hotel_id'] ) }}
        {{ Form::hidden('formData[token]', @$message['token'] ) }}
        {{ Form::hidden('method', 'Messages.replyHotel', [ 'id' => 'method' ] ) }}
        <button id="sendReplyMsg">{{ trans('global.btn_send') }}</button>
        {{ Form::close() }}

    </div>
@endsection

@section('scripts')


@endsection