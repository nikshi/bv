<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 * Date: 1/29/17
 * Time: 7:11 PM
 */


return [

    /*
    |--------------------------------------------------------------------------
    | Hotel Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'gallery' => "Галерия",
    'map' => "Карта",
    'contacts' => "Контакти",
    'search' => "Търси",
    'description' => "Описание",
    'rooms' => "Стаи",
    'facilities' => "Удобства",
    'hotel_facilities_title' => "Удобства в обекта (хотела)",
    'hotel_facilities_description' => "Тук са изброени екстрите на хотела, които са достъпни за всички гости на хотела.",
    'hotel_room_facilities_title' => "Удобства в стаите.",
    'hotel_room_facilities_description' => "Тези екстри се намират в различните стаи и могат да се ползват само от наемателите на стаята. В секция Стаи може да видите, коя стая, какви екстри предлага",
    'reviews' => "Отзиви и отценки",
    'room_price' => "Цена",
    'room_type' => "Вид",
    'room_persons' => "Брой наематели",
    'room_count' => "Брой стаи от този вид",
    'contact_information' => "Контактна информация",
    'contact_name' => "Лице за контакти",
    'phone' => "Телефон за връзка",
    'website' => "Уеб сайт",
    'area' => "Област",
    'municipality' => "Община",
    'city' => "Населено място",
    'address' => "Адрес",
    'coords' => "Кординати",
    'contact_us' => "Пишете ни",
    'reply' => "Отговор",
    'your_name' => "Име и фамилия",
    'your_email' => "Емайл",
    'your_phone' => "Телефон за връзка",
    'your_message' => "Съобщение",
    'message_successful_send' => "Съобщението е изпратено успешно",
    'email_subject_to_hotel' => "Ново съобщение в Obikolka.com",
    'email_to_hotel' => 'Здравейте :contact_name, <br> Имате ново съобщение от :guest_name. За да прочетете съобщението си влезнете в секция Съобщение на Вашия профил. За да влезнете в профила кликнете <a href="#" target="_blank">ТУК</a><br><br>Поздрави!<br><em>Екипът на Obikolka.com</em>',



];