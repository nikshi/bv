<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 * Date: 1/29/17
 * Time: 7:32 PM
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Main Dashboard Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'statistics' => 'Статистики',
    'profile' => 'Профил',
    'property' => 'Обект',
    'offers' => 'Оферти',
    'messages' => 'Съобщения',

];
