<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//FrontEnd routes
Route::get('/', 'HomepageController@init');
Route::get('/hotels', 'HotelsController@hotelsList');
Route::get('/hotels/{slug}', 'HotelsController@getHotel');
Route::get('/offers', 'FrontOffersController@init');
Route::get('/map', 'MapController@init');

//Hosts routes
Route::get('dashboard', 'StatisticsController@init');
Route::get('dashboard/hotel', 'HotelsController@createEditHotel');
Route::get('dashboard/profile', 'ProfileController@init');
Route::get('dashboard/offers', 'OffersController@init');
Route::get('dashboard/messages', 'MessagesController@init');
Route::get('dashboard/messages/{token}', 'MessagesController@chat');

//Ajax Api for get and set data
Route::post('ajax', 'AjaxController@event');

Route::get('api/get_header', 'ApiController@getHeader');


Route::get('/clear-cache', function() {
    $exitCode = \Illuminate\Support\Facades\Artisan::call('config:cache');
    echo '<pre><br>=====DEBUG START======<br> '.print_r($exitCode,1).' <br>=====DEBUG END======<br></pre>';
});

// Error 404
Route::get('{page}', function (){
//    TODO: Create 404 page
    echo "404 Страницата не е намерена";
})->where(['page' => '.*']);


