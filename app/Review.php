<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table    = 'reviews';
    protected $guarded  = ['id'];

    public static $validation_rules= [

        ];

    public static $messages = [
    ];

    public function hotel() {
        return $this->hasOne('App\Hotel');
    }
}
