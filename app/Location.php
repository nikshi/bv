<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table    = 'locations';
    protected $guarded  = ['id'];

    public function hotels() {
        return $this->hasMany('App\Hotel');
    }


}
