<?php

namespace App\Listeners;

use App\Events\StatisticsEvent;
use App\Services\StatisticsService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class StatisticsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StatisticsEvent  $event
     * @return void
     */
    public function handle(StatisticsEvent $event)
    {
        $method = $event->getMethod();
        $hotel_id = $event->getHotelId();

        $statistics = new StatisticsService;

        if(!method_exists($statistics, $method )){
//            TODO:: Write better decision
            echo "Method ".$method." not exist"; exit;
        }

        $statistics->$method($hotel_id);

    }
}
