<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessagesModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table    = 'messages';
    protected $guarded  = ['id'];


    public static $validation_rules
        = [
            'name'     => 'required',
            'email'    => 'required',
            'message'  => 'required',
        ];


    public static $messages = [

        'name.required'     => "Попълнете Вашето име",
        'email.required'    => 'Вашия е-мейл адрес е задължителен',
        'message.required'  => 'Моля напишете Вашето съобщение',

    ];

}
