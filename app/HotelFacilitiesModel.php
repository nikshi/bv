<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelFacilitiesModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table    = 'hotel_facilities';
    protected $guarded  = ['id'];


    public static $validation_rules
        = [

        ];


    public static $messages = [

        //TODO: write real messages

    ];

}
