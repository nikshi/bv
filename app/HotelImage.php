<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelImage extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table    = 'hotel_images';
    protected $guarded  = ['id'];



    public function hotel() {
        return $this->hasOne('App\Hotel');
    }


}
