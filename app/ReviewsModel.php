<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewsModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table    = 'reviews';
    protected $guarded  = ['id'];


    public static $validation_rules= [

        ];


    public static $messages = [
    ];

}
