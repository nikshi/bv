<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table    = 'hotel_types';
    protected $guarded  = ['id'];

    public function hotels() {
        return $this->hasMany('App\Hotel');
    }


}
