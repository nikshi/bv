<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table    = 'facilities';
    protected $guarded  = ['id'];

    /**
     * The facilities that belong to the hotel.
     */
    public function hotels()
    {
        return $this->belongsToMany('App\Hotel');
    }

    /**
     * The facilities that belong to the room.
     */
    public function rooms()
    {
        return $this->belongsToMany('App\Room');
    }
}
