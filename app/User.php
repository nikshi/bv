<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static $validation_rules
        = [
            'old_password'              => 'required',
            'password'                  => 'required|min:6',
            'password_confirmation'     => 'required|min:6|same:password',

        ];


    public static $messages = [

        //TODO: write real messages

        'required'     => 'Въведете парола',
        'min'          => 'Паролата трябва да бъде най-малко 6 символа',
        'same'         => 'Паролите не съвпадат',

    ];

    public function hotel(){
        return$this->hasMany('App\Hotel');
    }

}
