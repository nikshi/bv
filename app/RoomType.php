<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table    = 'room_types';
    protected $guarded  = ['id'];

    public function rooms() {
        return $this->hasMany('App\Room');
    }


}
