<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomsModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table    = 'rooms';
    protected $guarded  = ['id'];


    public static $validation_rules
        = [
            'room_type'     => 'required',
            'room_name'     => 'required',
            'room_persons'  => 'required|numeric',
            'room_price'    => 'required|numeric',
        ];


    public static $messages = [

        //TODO: write real messages

        'room_type.required'     => "Моля изберете вида на стаята",
        'room_name.required'     => 'Моля въведете име на стаята',
        'room_persons.required'  => 'Моля въведете, за колко души е стаята',
        'room_persons.numeric'   => 'Броят на гостите в помещението трябва да е число ',
        'room_price.required'    => 'Моля въведете цена за стаята',
        'room_price.numeric'     => 'В полето с цена трябва да има само цифри.',


    ];

}
