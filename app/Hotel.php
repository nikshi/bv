<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table    = 'hotels';
    protected $guarded  = ['id'];


    public static $validation_rules
        = [
            'name'           => 'required',
            'stars'          => 'required',
            'hotel_type'     => 'required',
            'description'    => 'required',
            'contact_name'   => 'required',
            'phone1'         => 'required|numeric',
            'phone2'         => 'numeric',
            'email'          => 'required|email',
            'website'        => 'url',
            'area'           => 'required|numeric',
            'municipality'   => 'required|numeric',
            'city'           => 'required',
            'distance_sea'   => 'numeric',
            'distance_ski'   => 'numeric',
            'hotel_lat'      => 'numeric',
            'hotel_lng'      => 'numeric',
        ];


    public static $messages = [
        'name.required'           => 'Името е задължително',
        'stars.required'          => 'Моля изберете, колко звезди е Вашия обект',
        'hotel_type.required'     => 'Моля изберете, какъв вид е Вашия обект',
        'description.required'    => 'Описанието на обекта е задължително',
        'contact_name.required'   => 'Моля попълнете полето "ЛИЦЕ ЗА КОНТАКТИ',
        'phone1.required'         => 'Моля попълнете полето "ТЕЛЕФОН"',
        'phone1.numeric'          => 'Полето "ТЕЛЕФОН" трябва да съдържа само числа',
        'phone2.numeric'          => 'Полето "ТЕЛЕФОН (АЛТЕРНАТИВЕН)" трябва да съдържа само числа',
        'email.required'          => 'Моля попълнете полето "ЕЛЕКТРОННА ПОЩА"',
        'email.email'             => 'Моля въведете коретна електронна поща',
        'website.url'             => 'Въвели сте неправилен уеб сайт',
        'area.required'           => 'Моля изберете "ОБЛАСТ"',
        'municipality.required'   => 'Моля изберете "ОБЩИНА"',
        'city.required'           => 'Моля попълнете "НАСЕЛЕНО МЯСТО"',
        'distance_sea.required'   => 'Разстоянието до плажа трябва да е число',
        'distance_ski.required'   => 'Разстоянието до ски пистата трябва да е число',
        'hotel_lat.required'      => 'Координатите трябва да са числа',
        'hotel_lng.required'      => 'Координатите трябва да са числа',
    ];

    /**
     * The facilities that belong to the hotel.
     */
    public function facilities()
    {
        return $this->belongsToMany('App\Facility', 'hotel_facilities', 'hotel_id', 'facility_id');
    }

    /**
     * The rooms that belong to the hotel.
     */
    public function rooms(){
        return $this->hasMany('App\Room');
    }

    /**
     * The owner/user of this hotel.
     */
    public function user(){
        return $this->belongsTo('App\User');
    }

    /**
     * The reviews that belong to the hotel.
     */
    public function reviews(){
        return $this->hasMany('App\Review');
    }

    public function type(){
        return $this->belongsTo('App\HotelType', 'hotel_type');
    }

    public function _area(){
        return $this->belongsTo('App\Location', 'area');
    }

    public function _municipality(){
        return $this->belongsTo('App\Location', 'municipality');
    }

    /**
     * The images that belong to the hotel.
     */
    public function images(){
        return $this->hasMany('App\HotelImage');
    }

}
