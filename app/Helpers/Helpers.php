<?php
/**
 * Created by PhpStorm.
 * User: Phoenix
 * Date: 6/19/2017
 * Time: 11:11 PM
 */

namespace App\Helpers;

class Helpers
{

    public static function makePagination($result, $page, $step) {
        $out = '';

        $out .= '<ul id="pagination">';
        for( $i = 1; $i <= ceil($result/$step); $i++) {
            if($i == $page){
                $out .= '<li class="active"><a>' . $i . '</a></li>';
            } else {
                $out .= '<li><a data-href="' . $i . '">' . $i . '</a></li>';
            }
        }
        $out .= '</ul>';
        return $out;
    }

    public static function pre_search_name($sname)
    {
        $sname_ = explode(" ", $sname);
        if (count($sname_) > 1) {
            $_sname_ = "";
            foreach ($sname_ as $sk => $sv) {
                $m = count($sname_) - 1;
                if ($sk == $m) $_sname_ .= $sv . "%"; // interval
                else $_sname_ .= $sv . "% "; // niama interval v kraia
            }
        } // $sname_ > 0
        else $_sname_ = '%' . $sname . '%';

        return $_sname_;
    }



    public static function makeConstantsCollection($array, $const){
        $result = [];
        $consts = config('consts.'.$const);
        foreach($consts as $key => $facility){
            if(in_array($key, $array)){
                $result[$key] = $facility;
            }
        }
        return $result;
    }

    public static function createSlug($name, $id){
        $translirateName    = strtolower(self::transliterate($name));
        $clearSpaces        = str_replace(' ', '-', $translirateName);
        $clearUnderScore    = str_replace('_', '-', $clearSpaces);
        $clearDot          = str_replace('.', '-', $clearUnderScore);
        $clearDots          = str_replace(':', '-', $clearDot);
        $finalString        = $id . '-' . preg_replace("/[^a-zA-Z-0-9]/", "", $clearDots);
        $res = Hotel::updateOrCreate(
            ['id'=>$id],
            ['slug' => $finalString] );
//        TODO:: check record and save in err log if not success
    }

    private static function transliterate($textcyr = null, $textlat = null) {
        $cyr = array(
            'ж',  'ч',  'щ',   'ш',  'ю',  'а', 'б', 'в', 'г', 'д', 'е', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ъ', 'ь', 'я',
            'Ж',  'Ч',  'Щ',   'Ш',  'Ю',  'А', 'Б', 'В', 'Г', 'Д', 'Е', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ъ', 'Ь', 'Я');
        $lat = array(
            'j', 'ch', 'sht', 'sh', 'iu', 'a', 'b', 'v', 'g', 'd', 'e', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'a', 'i', 'q',
            'J', 'Ch', 'Sht', 'Sh', 'Iu', 'A', 'B', 'V', 'G', 'D', 'E', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'A', 'I', 'Q');
        if($textcyr) return str_replace($cyr, $lat, $textcyr);
        else if($textlat) return str_replace($lat, $cyr, $textlat);
        else return null;
    }

}