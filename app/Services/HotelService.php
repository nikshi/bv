<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 * Date: 1/27/17
 * Time: 11:12 PM
 */

namespace App\Services;

use App\Events\StatisticsEvent;
use App\Helpers\Helpers;
use Validator;

use Illuminate\Support\Facades\Auth as Auth;
use Illuminate\Support\Str;
use App\Hotel as Hotel;
use App\Services\HotelFacilitiesService as HotelFacilitiesService;
use App\Services\RoomsFacilitiesService as RoomsFacilitiesService;
use App\Services\FilesService as FilesService;
use App\Services\RoomsService as RoomsService;

class HotelService extends BaseService
{

    private $vars = array();

    function __construct($vars = [])
    {
        $this->vars = $vars;
    }

    public function getHotelIdByUserId( $userId ){
        $hotel = Hotel::where('user_id', $userId)->first();
        if($hotel) return $hotel;
        else return false;
    }

    public function getRawDetails($hotelId){
        return Hotel::where('id', $hotelId)->first();
    }

    public function getHotelForMap($hotelId){
        $hotel = Hotel::where('id', $hotelId)->select('name','slug', 'hotel_type', 'description', 'hotel_lat', 'hotel_lng')->first();
        $hotel->link = url('hotels/'.$hotel->slug);
        $hotel->image = FilesService::getMainPhoto($hotelId);
        return $hotel;
    }

    public function getHotelIdBySlug($slug){
        return Hotel::where('slug', $slug)->select('id')->first();
    }

    public function save(){

        $HotelFacilitiesService = new HotelFacilitiesService;

        $validator = Validator::make($this->vars['mainInfo'], Hotel::$validation_rules, Hotel::$messages );

        if ($validator->fails()) {
            return array( "status" => -1, "status_txt" => $validator->messages()->first() );
        } else{

            $this->vars['mainInfo']['slug']     = '';
            $this->vars['mainInfo']['owner']    = $this->getOwner()->id;
            $this->vars['mainInfo']['status']   = 0;


            $res = Hotel::updateOrCreate(
                ['id'=>$this->vars['id']],
                $this->vars['mainInfo'] );

            if( $res->id ) {
                Helpers::createSlug( $this->vars['mainInfo']['name'], $res->id );
                $HotelFacilitiesService->save( $this->vars['facilities'], $res->id );
            }

            $res = [
                'status'        => 1,
                'status_txt'    => trans('hotels.hotel_save_successful'),
                'hotel_id'      => $res->id
            ];

            return $res;

        }
    }




//    private function getOwner(){
//        return Auth::user();
//    }


    public function getFeaturedHotels(){
        $featuredHotels = array();
        $params = array('status' => 2);
        $featuredHotels = $this->getHotels($params);

        return $featuredHotels;
    }

    public function getRandomHotels($count = 10){
        $randomHotels = array();
        $randomHotels = $this->getHotels();
        if($count != -1){
            $randomHotels = array_slice($randomHotels, 0, $count);
        }
        shuffle($randomHotels);
        return $randomHotels;
    }

    public function getSeaHotels($count = 10){
        $params['main']['distance_sea'] = 30000;
        $seaHotels = array();
        $seaHotels = $this->getHotels($params);
        if($count != -1) {
            $seaHotels = array_slice($seaHotels, 0, $count);
        }
        shuffle($seaHotels);
        return $seaHotels;
    }

    public function getHotelsByMaxPrice($maxPrice, $count = 10){
        $params['rooms']['price_max'] = $maxPrice;
        $hotels = array();
        $hotels = $this->getHotels($params);
        if($count != -1) {
            $hotels = array_slice($hotels, 0, $count);
        }
        shuffle($hotels);
        return $hotels;
    }

    public function getSearchedHotels(){
        $params = $this->vars;
        $hotels_id = [];
        $step = 10;
        $start  = ($params['page'] * $step) - $step;

        $hotelSsResult  = '';

        $hotels = $this->getHotels($params);

        //Pagination - get results per page
        $hotels_ids = array_slice($hotels, $start, $step);

        foreach ($hotels_ids as $hotel) {
            $hotelSsResult .= $this->getHotelListHtml( $hotel );
            $hotels_id[] = $hotel['main']['id'];
        }

        event(new StatisticsEvent('searchedHotel', $hotels_id));

        $pagination = parent::makePagination(count($hotels), $params['page'], $step);
        $hotelResultWithPagination = $hotelSsResult . $pagination;

        $result = [
            'status'        => 1,
            'count'         => count($hotels),
            'data'          => $hotelResultWithPagination,
        ];

        return $result;
    }

    public function getSearchedMapHotels(){
        $params = $this->vars;

        $hotels = $this->getHotelsMainInfo($params);

//        event(new StatisticsEvent('searchedHotel', $hotels_id));

        $result = [
            'status'        => 1,
            'count'         => count($hotels),
            'data'          => $hotels,
        ];

        return $result;
    }

    private function getHotels($params = array()){

        $hotels = array();

//        Hotel Filtering
        $_Hotels_ids = $this->hotelsSearchFilters($params);

        foreach ($_Hotels_ids as $hotel_id) {
            $hotels[] = $this->getHotelDetails($hotel_id);
        }

        //Ordering
        if( isset( $params['ordering']) && isset( $params['ordering']) != '' ) {
            $hotels = $this->hotels_ordering($hotels, $params['ordering']);
        }

        return $hotels;
    }

    private function getHotelsMainInfo($params = array()){

        $hotels = array();

//        Hotel Filtering
        $_Hotels_ids = $this->hotelsSearchFilters($params);

        foreach ($_Hotels_ids as $key => $hotel_id) {
            $hotels[] = $this->getHotelForMap($hotel_id);
        }

        return $hotels;
    }

    public function getHotelDetails( $hotel_id ) {

        $reviewsService = new ReviewsService();

        $hotel_rooms_facilities = [];

        $hotel['main']      = $this->getRawDetails( $hotel_id )->toArray();
        $_hotel_facilities  = HotelFacilitiesService::getHotelFacilitiesIds( $hotel_id );
        $hotel_facilities   = BaseService::makeConstantsCollection($_hotel_facilities, 'hotel_facilities');
        $rooms              = RoomsService::getRawDetails($hotel_id)->toArray();
        $hotel['main']['area']          = config('consts.locate_area')[$hotel['main']['area']];
        $hotel['main']['municipality']  = config('consts.locate_municipalities')[$hotel['main']['municipality']];
        $hotel['main']['lowest_price']  = PHP_INT_MAX;
        $hotel['main']['ratings']       = $reviewsService->getHotelRatings($hotel_id);

        foreach ( $rooms as &$room ) {
            if($room['room_price'] < $hotel['main']['lowest_price']) {
                $hotel['main']['lowest_price'] = $room['room_price'];
            }

            if($room['room_persons'] == 1){
                if(isset($hotel['main']['single_room_price'])){
                    if($room['room_price'] < $hotel['main']['single_room_price']){
                        $hotel['main']['single_room_price'] = $room['room_price'];
                    }
                } else {
                    $hotel['main']['single_room_price'] = $room['room_price'];
                }
            }
            if($room['room_persons'] == 2){
                if(isset($hotel['main']['double_room_price'])){
                    if($room['room_price'] < $hotel['main']['double_room_price']){
                        $hotel['main']['double_room_price'] = $room['room_price'];
                    }
                } else {
                    $hotel['main']['double_room_price'] = $room['room_price'];
                }
            }

            $room['room_type'] = config('consts.room_types')[$room['room_type']];
            $room['facilities'] = BaseService::makeConstantsCollection($room['facilities'], 'room_facilities');

            foreach ($room['facilities'] as $key => $value){
                if(!key_exists($key,$hotel_rooms_facilities)){
                    $hotel_rooms_facilities[$key] = $value;
                }
            }
        }

//      Set Average price
        if (isset($hotel['main']['double_room_price'])){
            $hotel['main']['average_price'] = $hotel['main']['double_room_price'] / 2;
        } elseif (isset($hotel['main']['single_room_price'])) {
            $hotel['main']['average_price'] = $hotel['main']['single_room_price'];
        }

//        $hotel['main']                          = $hotel;
        $hotel['hotel_facilities']              = $hotel_facilities;
        $hotel['hotel_rooms_facilities']        = $hotel_rooms_facilities;
        $hotel['hotel_main_photo']              = FilesService::getMainPhoto($hotel_id);
        $hotel['hotel_gallery']['original']     = FilesService::getGalleryImages($hotel_id);
        $hotel['hotel_gallery']['medium']       = FilesService::getGalleryImages($hotel_id, 'medium');
        $hotel['hotel_gallery']['thumbnail']    = FilesService::getGalleryImages($hotel_id,'thumbnail');
        $hotel['rooms']                         = $rooms;

        return $hotel;
    }

    public function getHotelListHtml( $hotel ){

        $result = '';
        $prices = '';
        if(isset($hotel['main']['single_room_price'])){
            $prices .= '<span class="price single-room"><small>' . trans('hotels.single_room') .'</small>'.$hotel['main']['single_room_price']. trans('global.currency_small').'</span>';
        }
        if(isset($hotel['main']['double_room_price'])){
            $prices .= '<span class="price double-room"><small>' . trans('hotels.double_room') .'</small>'.$hotel['main']['double_room_price']. trans('global.currency_small').'</span>';
        }

        $result = '<article class="box">
            <figure class="col-sm-5 col-md-4">
                <a title="'.$hotel['main']['name'].'" href="'.url('hotels/'.$hotel['main']['slug']).'" class="hover-effect"><img width="270" height="160" alt="" src="'.$hotel['hotel_main_photo'].'"></a>
            </figure>
            <div class="details col-sm-7 col-md-8">
                <div>
                    <div>
                        <h4 class="box-title"><a href="'.url('hotels/'.$hotel['main']['slug']).'" title="'.$hotel['main']['name'].'">'.$hotel['main']['name'].'</a><small><i class="soap-icon-departure yellow-color"></i> '.$hotel['main']['area'].', '.$hotel['main']['municipality'].', '.$hotel['main']['city'].'</small></h4>
                    </div>
                    <div>
                        <div class="five-stars-container">
                            <span class="five-stars" style="width: '.$hotel['main']['ratings']['percent']['average'].'%;"></span>
                        </div>
                        <span class="review">'. $hotel['main']['ratings']['count'] .' ' .  ($hotel['main']['ratings']['count'] == 1 ? trans('hotels.review') : trans('hotels.reviews')) .'</span>
                    </div>
                </div>
                <div>
                    <p>'. Str::words($hotel['main']['description'], 40,'....').'</p>
                    <div>
                        '.$prices.'
                        <a class="button btn-small full-width text-center" title="" href="'.url('hotels/'.$hotel['main']['slug']).'">' . trans('hotels.view') .'</a>
                    </div>
                </div>
            </div>
        </article>';

        return $result;
    }

//    Helper Functions
    private function hotelsSearchFilters($params){

        $_Hotels_ids = array();

        $Hotel = Hotel::select('hotels.id')->groupBy('hotels.id');
        $Hotel = $Hotel->join('hotel_facilities', 'hotels.id', '=', 'hotel_facilities.hotel_id');
        $Hotel = $Hotel->join('rooms', 'hotels.id', '=', 'rooms.hotel_id');

        if(isset($params['status'])){
            $Hotel->where('status', $params['status']);
        } else {
            $Hotel->where('status' ,'>=', 1);
        }

        if (isset($params['main']['name']) && !empty($params['main']['name']))
            $Hotel->where('hotels.name', 'LIKE', parent::pre_search_name($params['main']['name']));

        if (isset($params['main']['hotel_type']) && count($params['main']['hotel_type']) > 0)
            $Hotel->whereIn('hotels.hotel_type', $params['main']['hotel_type']);

        if (isset($params['main']['stars']) && count($params['main']['stars']) > 0)
            $Hotel->whereIn('hotels.stars', $params['main']['stars']);

        if (isset($params['main']['area']) && $params['main']['area'] > 0)
            $Hotel->where('hotels.area', $params['main']['area']);

        if (isset($params['main']['municipality']) && $params['main']['municipality'] > 0)
            $Hotel->where('hotels.municipality', $params['main']['municipality']);

        if (isset($params['main']['city']) && !empty($params['main']['city']))
            $Hotel->where('hotels.city', 'LIKE', parent::pre_search_name($params['main']['city']));

        if (isset($params['main']['distance_sea']) && !empty($params['main']['distance_sea'])){
            $Hotel->where('hotels.distance_sea', '<=', (int)$params['main']['distance_sea']);
            $Hotel->where('hotels.distance_sea', '>', 0);
        }
        if (isset($params['main']['distance_ski']) && !empty($params['main']['distance_ski'])) {
            $Hotel->where('hotels.distance_ski', '<=', $params['main']['distance_ski']*1000);
            $Hotel->where('hotels.distance_ski', '>', 0);
        }

        if (isset($params['rooms']['price_max']) && !empty($params['rooms']['price_max'])) {
            if(isset($params['rooms']['price_min'])){
                $Hotel->where('rooms.room_price', '>=', $params['rooms']['price_min']);
            }
            $Hotel->where('rooms.room_price', '<=', $params['rooms']['price_max']);
        }

        $_Hotels_ids = $Hotel->pluck('id')->toArray();

        //Facilities Filters
        if ( isset($params['hotel_facilities']) && count($params['hotel_facilities']) > 0 ) {
            $_Hotels_ids = HotelFacilitiesService::hotelFacilitiesFilter( $_Hotels_ids, $params['hotel_facilities'] );
        }

        if ( isset($params['hotel_room_facilities']) && count($params['hotel_room_facilities']) > 0 ) {
            $_Hotels_ids = RoomsFacilitiesService::roomFacilitiesFilter( $_Hotels_ids, $params['hotel_room_facilities'] );
        }

        return $_Hotels_ids;

    }




    private function hotels_ordering($hotels, $ordering){

        switch ($ordering){
            case 'name_asc':
                usort($hotels, function($a, $b) {
                    return strcasecmp($a['main']['name'], $b['main']['name']);
                });
                break;
            case 'name_desc':
                usort($hotels, function($a, $b) {
                    return strcasecmp($b['main']['name'], $a['main']['name']);
                });
                break;
            case 'price_asc':
                usort($hotels, function($a, $b) {
                    return $a['main']['average_price'] - $b['main']['average_price'];
                });
                break;
            case 'price_desc':
                usort($hotels, function($a, $b) {
                    return $b['main']['average_price'] - $a['main']['average_price'];
                });
                break;
            case 'rating_desc':
                break;
            case 'rating_asc':
                break;
            case 'views_desc':
                break;
            case 'views_asc':
                break;
        }

        return $hotels;

    }

}