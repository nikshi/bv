<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 * Date: 1/27/17
 * Time: 11:12 PM
 */

namespace App\Services;

use Validator;
use App\RoomFacilitiesModel as RoomFacilitiesModel;

class RoomsFacilitiesService extends BaseService
{

    private $vars = array();

    function __construct($vars = [])
    {
        $this->vars = $vars;
    }

    public static function getRoomFacilitiesIds($roomId){

        $roomFacilitiesIds = RoomFacilitiesModel::where('room_id', $roomId )->get(['facility_id']);
        $result = [];

        foreach($roomFacilitiesIds as $roomFacilityId) {
            $result[] = $roomFacilityId->facility_id;
        }

        return $result;
    }

    public function delete($room_id){
        $res = RoomFacilitiesModel::where('room_id', $room_id)->delete();
        return $res;
    }


    public function save($facilities, $roomId, $hotelId){

        $roomFacilitiesModel = new RoomFacilitiesModel;

//        $validator = Validator::make($formData, HotelModel::$validation_rules, HotelModel::$messages );

//        if ($validator->fails()) {
//            return array( "status" => -1, "status_txt" => $validator->messages()->first() );
//        } else{
//
//        }

        foreach ($facilities as $facility_id => $facility_value) {
            $facilitiesRecArr = [];
            $facilitiesRecArr['hotel_id']       = $hotelId;
            $facilitiesRecArr['room_id']        = $roomId;
            $facilitiesRecArr['facility_id']    = $facility_id;
            $facilitiesRecArr['facility_value'] = $facility_value;

            RoomFacilitiesModel::updateOrCreate(
                ['room_id' => $roomId, 'facility_id'=> $facility_id],
                $facilitiesRecArr
            );
        }

        $deletedRows = RoomFacilitiesModel::where('facility_value', 0)->delete();

//        return $result;
    }

    public static function roomFacilitiesFilter($hotels_ids, $facilities_ids){
        $tempHotelIds = array();
        $rooms = RoomFacilitiesModel::select('room_id', 'hotel_id')->whereIn('hotel_id', $hotels_ids)->groupBy('room_id')->get()->toArray();
        foreach ($rooms as $room){
            $isTheSerachedRoom = true;
            foreach ($facilities_ids as $facility_id) {
                $RoomFacilitiesModel = RoomFacilitiesModel::select('hotel_id');
                $RoomFacilitiesModel->where('room_id', $room['room_id']);
                $RoomFacilitiesModel->where('facility_id', $facility_id);
                $RoomFacilitiesModel->where('facility_value', 1);
                $roomsWithFacility = $RoomFacilitiesModel->pluck('hotel_id')->toArray();
                if(!$roomsWithFacility) $isTheSerachedRoom = false;
            }
            if($isTheSerachedRoom) $tempHotelIds[$room['room_id']] = $room['hotel_id'];
        }
        foreach ($hotels_ids as $key => $hotelId){
            if(!in_array($hotelId,$tempHotelIds)){
                unset($hotels_ids[$key]);
            }
        }
        return $hotels_ids;
    }

}