<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 * Date: 1/27/17
 * Time: 11:12 PM
 */

namespace App\Services;

use Validator;

use Illuminate\Support\Facades\Auth as Auth;
use App\Room as Room;
use App\Services\RoomsFacilitiesService as RoomsFacilitiesService;

class RoomsService extends BaseService
{

    private $vars = array();

    function __construct($vars = [])
    {
        $this->vars = $vars;
    }


    public function getRoomsHtml(){
        $result     = '';
        $hotelId    = $this->vars['hotel_id'];
        $rooms      = self::getRawDetails($hotelId);

        $room_facilities    = config('consts.room_facilities');
        $room_types         = config('consts.room_types');

        foreach ($rooms as $room ) {
            $facilities = '';


            foreach ($room->facilities as $facility){
                $facilities .= '<li class="room_value" data-name="facilities" data-value="' . $facility . '"><i class="fa fa-check" aria-hidden="true"></i>' . $room_facilities[$facility] . '</li>';
            }

            $result .= '<div class="room">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="room_label"> ' . trans('hotels.room_list_name'). '</div>
                                        <div class="room_value" data-name="room_name" data-value="' . $room->room_name . '">' . $room->room_name . '</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="room_label">'.trans('hotels.room_list_type').'</div>
                                        <div class="room_value" data-name="room_type" data-value="'.$room->room_type.'">'.$room_types[$room->room_type].'</div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="room_label">'.trans('hotels.room_list_persons').'</div>
                                        <div class="room_value" data-name="room_persons" data-value="'.$room->room_persons.'">'.$room->room_persons.'</div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="room_label">'.trans('hotels.room_count').'</div>
                                        <div class="room_value" data-name="room_count" data-value="'.$room->room_count.'">'.$room->room_count.'</div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="room_label">' . trans('hotels.room_list_price') . '</div>
                                        <div class="room_value" data-name="room_price" data-value="' . $room->room_price . '">' . $room->room_price . '</div>
                                    </div>
                                    <div class="col-md-3">
                                        <a class="button btn-small yellow" id="btnEditRoom" href="#" >'.trans('hotels.btn_edit_room').'</a>
                                        <a class="button btn-small red" data-room-id="' . $room->id . '" id="btnDelRoom" href="#" >'.trans('hotels.btn_del_room').'</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="room_label">'.trans('hotels.room_list_description').'</div>
                                        <div class="room_value" data-name="room_description" data-value="'.$room->room_description.'">' . $room->room_description . '</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="room_label">' . trans('hotels.room_list_extras') . '</div>
                                        <ul class="room_extras">' . $facilities . '</ul>

                                    </div>
                                </div>
                                <input class="room_value" type="hidden"  data-name="room_id" data-value="' . $room->id . '">

                            </div>';
        }

        return $result;

    }

    public static function getRawDetails($hotelId){

      $rooms =   Room::where('hotel_id', $hotelId)->orderBy('id', 'desc')->get();

      foreach ( $rooms as &$room ) {
          $facilities = RoomsFacilitiesService::getRoomFacilitiesIds($room->id);
        $room['facilities'] = $facilities;
      }

      return $rooms;

    }


    public function delete(){

        $roomId    = $this->vars['room_id'];
        if( !isset($this->vars['room_id']) || $roomId <= 0) {
            $return = [
                'status' => -1,
                'status_txt' => trans('hotels.room_not_found'),

            ];
            return json_encode($return);
        }
        $RoomsFacilitiesService = new RoomsFacilitiesService;

        $res_del_room = Room::where('id', $roomId)->delete();

        if( $res_del_room > 0 ){
            $rel_room_fac =  $RoomsFacilitiesService->delete($roomId);
        } else {
            $return = [
                'status' => -2,
                'status_txt' => trans('hotels.room_del_problem'),

            ];
            return $return;
        }

        $return = [
            'status' => 1,
            'status_txt' => trans('hotels.room_del_successful'),

        ];

        return $return;
    }

    public function save(){

            $RoomsFacilitiesService = new RoomsFacilitiesService;

        $validator = Validator::make($this->vars['roomInfo'], Room::$validation_rules, Room::$messages );

        if ($validator->fails()) {
            return array( "status" => -1, "status_txt" => $validator->messages()->first() );
        } else{

            $this->vars['roomInfo']['hotel_id'] = $this->vars['hotel_id'];

            $res = Room::updateOrCreate(
                ['id'=>$this->vars['room_id']],
                $this->vars['roomInfo']);
            if( $res->id ) {
                $RoomsFacilitiesService->save( $this->vars['facilities'], $res->id, $this->vars['hotel_id'] );
            }

            $res = [
                'status'        => 1,
                'status_txt'    => trans('hotels.room_save_successful')
            ];

            return $res;

        }


    }


}