<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 * Date: 1/27/17
 * Time: 11:12 PM
 */

namespace App\Services;

use ClassPreloader\Config;
use Validator;

use Illuminate\Support\Facades\Auth as Auth;
use App\MessagesModel as MessagesModel;
use App\Services\HotelService as HotelService;
use App\Classes\UploadHandler as UploadHandler;

class MessagesService extends BaseService
{

    private $vars = array();

    function __construct($vars = [])
    {
        $this->vars = $vars;
    }


    public function getMessages($hotel_id){
        $user = Auth::user();
        $_messages = MessagesModel::select('*')->where('hotel_id', $hotel_id)->where('email','!=', $user->email)->where('status', '>=', 0)->orderBy('updated_at', 'desc')->get()->toArray();
        $messages = [];
        foreach ($_messages as $message){
            if(array_key_exists($message['token'], $messages)) {
                if($message['updated_at'] > $messages[$message['token']]['updated_at']) {
                    $messages[$message['token']] = $message;
                }
            } else {
                $messages[$message['token']] = $message;
            }
        }

        return $messages;
    }

    public function getMessagesByToken($token){
        $messages = MessagesModel::select('*')->where('token', $token)->get()->toArray();
        return $messages;
    }

    public function markAsRead($message){
        $message['status'] = 1;
        MessagesModel::where( 'token', $message['token'])->where('status', 0)->update(['status' => 1]);
    }

    public function sendToHotel(){

        $data           = $this->vars;
        $data['token']  = $this->createToken($data['hotel_id'], $data['email'] );
        $data['status'] = 0;

        $validator = Validator::make($data, MessagesModel::$validation_rules, MessagesModel::$messages );

        if ($validator->fails()) {
            return array( "status" => -1, "status_txt" => $validator->messages()->first() );
        } else{

            $res = MessagesModel::updateOrCreate( ['id' => $data['name']], $data );

            if($res) {
                $this->sendEmailToHotel($data);
            }

            $res = [
                'status'        => 1,
                'status_txt'    => trans('hotel.message_successful_send'),
            ];


        }

        return $res;

    }

    public function replyHotel(){

        $user = Auth::user();

        $data           = $this->vars;
        $data['status'] = 1;
        $data['email']  = $user->email;
        $data['name']   = $user->name;
        $data['phone']  = '';
        $validator = Validator::make($data, MessagesModel::$validation_rules, MessagesModel::$messages );

        if ($validator->fails()) {
            return array( "status" => -1, "status_txt" => $validator->messages()->first() );
        } else{

            $res = MessagesModel::updateOrCreate( ['id' => $data['name']], $data );

            if($res) {
                $this->sendEmailToClient($data);
            }

            $res = [
                'status'        => 1,
                'status_txt'    => trans('hotel.message_successful_send'),
            ];

        }

        return $res;

    }



    private function sendEmailToHotel($data){
        $hotelService = new HotelService;
        $hotel = $hotelService->getRawDetails($data['hotel_id']);
        $subject = trans('hotel.email_subject_to_hotel');
        $message = trans('hotel.email_to_hotel', ['contact_name' => $hotel->contact_name, 'guest_name' => $data['name']]);

    }

    private function sendEmailToClient($data){


//        TODO:: sendEmailToClient

//        $hotelService = new HotelService;
//        $hotel = $hotelService->getRawDetails($data['hotel_id']);
//        $subject = trans('hotel.email_subject_to_hotel');
//        $message = trans('hotel.email_to_hotel', ['contact_name' => $hotel->contact_name, 'guest_name' => $data['name']]);
    }

    private function createToken($hotel_id, $email){
        return base64_encode($hotel_id.'/'.$email);
    }

}