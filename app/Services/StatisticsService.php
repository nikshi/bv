<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 * Date: 1/27/17
 * Time: 11:12 PM
 */

namespace App\Services;


use App\Hotel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;

class StatisticsService extends BaseService
{

    public function getTopViewedWeekHotels($count = 10){

        $d =  strtotime('this week', time());

        return DB::table('stats_viewed')
            ->select(DB::raw('count(*) as count,hotel_id'))
            ->groupBy('hotel_id')
            ->where('added_on', '>', date('Y-m-d 00:00:00', $d))
            ->orderBy('count', 'desc')
            ->limit($count)
            ->get();
    }

    public function getTopSearchWeekHotels($count = 10){

        $d =  strtotime('this week', time());

        return DB::table('stats_search')
            ->select(DB::raw('count(*) as count,hotel_id'))
            ->groupBy('hotel_id')
            ->where('added_on', '>', date('Y-m-d 00:00:00', $d))
            ->orderBy('count', 'desc')
            ->limit($count)
            ->get();
    }

    public function searchedHotel($hotels_id){
        foreach ($hotels_id as $hotel_id){
            Hotel::where('id', $hotel_id)->increment('searched');
            DB::table('stats_search')->insert(['hotel_id' => $hotel_id]);
        }
    }

    public function viewHotel($hotel_id){
        if(!$this->isHotelViewed($hotel_id) ) {
            $this->storeHotelView($hotel_id);
            DB::table('stats_viewed')->insert(['hotel_id' => $hotel_id]);
            Hotel::where('id', $hotel_id)->increment('viewed');
        }
    }

    private function isHotelViewed($hotel_id)
    {
        $viewHotels = Cookie::get("asd")['viewHotels'];
        if(!is_array($viewHotels)) return 0;
        return in_array($hotel_id, $viewHotels);
    }

    private function storeHotelView($hotel_id)
    {
        $viewHotels = Cookie::get("asd")['viewHotels'];

        if(!$viewHotels){
            Cookie::queue('asd', ['viewHotels' => [$hotel_id]], 5);
        } else {
            array_push($viewHotels, $hotel_id);

            Cookie::queue('asd', ['viewHotels' => $viewHotels], 5);
        }

    }


}