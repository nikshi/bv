<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 * Date: 1/27/17
 * Time: 11:12 PM
 */

namespace App\Services;

use Validator;
use App\HotelFacilitiesModel as HotelFacilitiesModel;
use App\RoomFacilitiesModel as RoomFacilitiesModel;

class HotelFacilitiesService extends BaseService
{

    private $vars = array();

    function __construct($vars = [])
    {
        $this->vars = $vars;
    }

    public static function getHotelFacilitiesIds($hotelId){
        $hotelFacilitiesIds = HotelFacilitiesModel::where('hotel_id', $hotelId )->get(['facility_id']);
        $result = [];

        foreach($hotelFacilitiesIds as $hotelFacilityId) {
            $result[] = $hotelFacilityId->facility_id;
        }

        return $result;
    }


    public function save($facilities, $hotelId){

        $hotelFacilitiesModel = new HotelFacilitiesModel;

//        $validator = Validator::make($formData, HotelModel::$validation_rules, HotelModel::$messages );

//        if ($validator->fails()) {
//            return array( "status" => -1, "status_txt" => $validator->messages()->first() );
//        } else{
//
//        }

        foreach ($facilities as $facility_id => $facility_value) {
            $facilitiesRecArr = [];
            $facilitiesRecArr['hotel_id']       = $hotelId;
            $facilitiesRecArr['facility_id']    = $facility_id;
            $facilitiesRecArr['facility_value'] = $facility_value;

            $hotelFacilitiesModel::updateOrCreate(
                ['hotel_id' => $hotelId, 'facility_id'=> $facility_id],
                $facilitiesRecArr
            );
        }

        $deletedRows = $hotelFacilitiesModel::where('facility_value', 0)->delete();

//        return $result;
    }

    private function create(){

//        echo '<pre><br>=====DEBUG START======<br> '.print_r($this->vars,1).' <br>=====DEBUG END======<br></pre>';
        //return HotelFacilitiesModel::create($this->vars);
    }

    private function update(){

    }




//    Filters
    public static function hotelFacilitiesFilter($hotels_ids, $facilities_ids){
        foreach ($facilities_ids as $facility_id) {
            $HotelFacilitiesModel = HotelFacilitiesModel::select('hotel_id');
            $HotelFacilitiesModel->whereIn('hotel_id', $hotels_ids);
            $HotelFacilitiesModel->where('facility_id', $facility_id);
            $HotelFacilitiesModel->where('facility_value', 1);
            $hotelsWithFacility = $HotelFacilitiesModel->pluck('hotel_id')->toArray();
            foreach ($hotels_ids as $key => $hotelId){
                if(!in_array($hotelId,$hotelsWithFacility)){
                    unset($hotels_ids[$key]);
                }
            }
        }
        return $hotels_ids;
    }

    public static function filter_field_d30($ids, $value)
    {
        $Monuments_ids = array();

        $MonumentsParams = MonumentsParams::select('monuments_id');

        $MonumentsParams->whereIn('monuments_id', $ids);
        $MonumentsParams->where('monument_key', 'field_d30');
        $MonumentsParams->where('value', 'LIKE', F::pre_search_name($value));

        $_Monuments_ids = $MonumentsParams->get();

        $Monuments_ids = [];
        foreach ($_Monuments_ids as $_Monuments_id) {
            $Monuments_ids[] = $_Monuments_id->monuments_id;
        }

        return $Monuments_ids;
    }


}