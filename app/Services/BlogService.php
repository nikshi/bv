<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 * Date: 1/27/17
 * Time: 11:12 PM
 */

namespace App\Services;


class BlogService extends BaseService
{

    /**
     * @param $cat_id
     * @param int $count
     * @param string $status
     * @return array
     */

    public function getBlogPosts($cat_id, $count = 5, $status = 'publish'){
        $posts = [];

        $_posts = file_get_contents('https://obikolka.com/blog/wp-json/wp/v2/posts?status='.$status.'&per_page='.$count.'&categories='.$cat_id);

        foreach (json_decode($_posts) as $key => $post){
            $posts[$key]['title']       = $post->title->rendered;
            $posts[$key]['link']        = $post->link;
            $posts[$key]['date']        =  $post->date;
            $posts[$key]['modified']    = $post->modified;
            $posts[$key]['excerpt']     = trim(strip_tags($post->excerpt->rendered));
            if($post->featured_media){
                $posts[$key]['image'] = json_decode(file_get_contents('https://obikolka.com/blog/wp-json/wp/v2/media/'.$post->featured_media))
                    ->media_details
                    ->sizes->medium
                    ->source_url;
            } else {
                $posts[$key]['image'] = '';
            }
        }
        return $posts;
    }

}