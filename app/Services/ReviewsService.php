<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 * Date: 1/27/17
 * Time: 11:12 PM
 */

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Facades\Auth as Auth;
use App\Review as Review;

class ReviewsService extends BaseService
{

    private $vars = array();

    function __construct($vars = [])
    {
        $this->vars = $vars;
    }


    public function getReviews($hotel_id){

        $reviews = Review::where('hotel_id', $hotel_id)->where('status', 0)->orderBy('id', 'desc')->get();

        foreach ($reviews as &$review){
            $categories              = 6;
            $review->avgRating = 0;
            $review->avgRatingPercents = 0;
            if( $review->service <=  0 ) $categories--;
            if( $review->food <=  0 ) $categories--;
            if( $review->relax <=  0 ) $categories--;
            if( $review->location <=  0 ) $categories--;
            if( $review->price <=  0 ) $categories--;
            if( $review->clean <=  0 ) $categories--;

            if($categories > 0) {
                $review->avgRating = round(($review->service + $review->food + $review->relax + $review->location + $review->price + $review->clean) / $categories, 1);
                $review->avgRatingPercents = ( $review->avgRating / 5 ) * 100;
            }

        }

        return $reviews;

    }

    public function getHotelRatings($hotel_id){

        $categories         = 6;
        $num_average        = 0;
        $percent_average    = 0;

//        $relax = Review::where('hotel_id', $hotel_id)
//            ->select(DB::raw('avg(service) as service,
//                              avg(food) as food,
//                              avg(relax) as relax,
//                              avg(location) as location,
//                              avg(price) as price,
//                              avg(clean) as clean '))
//            ->first();

        $service    = Review::where('hotel_id', $hotel_id)->where('service', '>', 0)->where('status', '>', 0)->avg('service');
        $food       = Review::where('hotel_id', $hotel_id)->where('food', '>', 0)->where('status', '>', 0)->avg('food');
        $relax      = Review::where('hotel_id', $hotel_id)->where('relax', '>', 0)->where('status', '>', 0)->avg('relax');
        $location   = Review::where('hotel_id', $hotel_id)->where('location', '>', 0)->where('status', '>', 0)->avg('location');
        $price      = Review::where('hotel_id', $hotel_id)->where('price', '>', 0)->where('status', '>', 0)->avg('price');
        $clean      = Review::where('hotel_id', $hotel_id)->where('clean', '>', 0)->where('status', '>', 0)->avg('clean');
        $count      = Review::where('hotel_id', $hotel_id)->where('status', '>', 0)->count();

        if( $service <=  0 ) $categories--;
        if( $food <=  0 ) $categories--;
        if( $relax <=  0 ) $categories--;
        if( $location <=  0 ) $categories--;
        if( $price <=  0 ) $categories--;
        if( $clean <=  0 ) $categories--;

        if($categories > 0) {
            $num_average = ($service + $food + $relax + $location + $price + $clean) / $categories;
        }

        return [
            'numbers'=> [
                'service'   => $service,
                'food'      => $food,
                'relax'     => $relax,
                'location'  => $location,
                'price'     => $price,
                'clean'     => $clean,
                'average'   => round($num_average, 1)
            ],
            'percent' => [
                'service'   => ( $service / 5 ) * 100,
                'food'      => ( $food / 5 ) * 100,
                'relax'     => ( $relax / 5 ) * 100,
                'location'  => ( $location / 5 ) * 100 ,
                'price'     => ( $price / 5 ) * 100,
                'clean'     => ( $clean / 5 ) * 100,
                'average'   => ( $num_average / 5 ) * 100
            ],
            'count' => $count
        ];

    }

    public function writeReview(){

        $validator = Validator::make($this->vars, Review::$validation_rules, Review::$messages );

        if ($validator->fails()) {
            $result = ["status" => -1, "status_txt" => $validator->messages()->first()];
        } else{
            $res = Review::updateOrCreate(['id' => 0], $this->vars);

            if( $res->id ) {
                $result = ['status' => 1, 'status_txt' => 'Коментара е изпратен успешно и ще бъде публикуван след преглед от модератор', 'data' => json_encode($this->vars)];
            } else {
                $result = ['status' => -1, 'status_txt' => 'Възникна проблем при записването на ревюто'];
            }
        }
        return $result;

    }

}