<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 * Date: 1/27/17
 * Time: 11:12 PM
 */

namespace App\Services;

use Validator;

use Illuminate\Support\Facades\Auth as Auth;
use App\HotelModel as HotelModel;
use App\HotelImage as HotelImage;
use App\Classes\UploadHandler as UploadHandler;

class FilesService extends BaseService
{

    private $vars = array();
    private $temp;

    function __construct($vars = [])
    {
        $this->vars = $vars;
    }



    public function uploadMainPhoto(){
        $result = [];
        $hotel_id       = $this->vars['hotel_id'];
        $fileName       = basename($_FILES['formData']['name']['mainPhoto']);
        $fileError      = $_FILES['formData']['error']['mainPhoto'];
        $fileTmpName    = $_FILES['formData']['tmp_name']['mainPhoto'];

        $target_dir     = self::getHotelImagesDirPath($hotel_id) . "main/";

        if ( !file_exists($target_dir) ) {
            mkdir($target_dir, 0777, true);
        }
        $target_file = $target_dir . $fileName;

        if( $fileError == UPLOAD_ERR_OK ) {

            $info = getimagesize($fileTmpName);

            if ($info === FALSE) {
                return [
                   'status'         => -1,
                   'status_txt'    => trans('hotels.hotel_upload_mainphoto_type_e')
                ];
            }
            if ( ($info[2] !== IMAGETYPE_JPEG) && ($info[2] !== IMAGETYPE_PNG)) {
                return [
                    'status'        => -1,
                    'status_txt'    => trans('hotels.hotel_upload_mainphoto_type_e')
                ];
            }

            if ($this->deleteMainPhoto($hotel_id) && move_uploaded_file($fileTmpName, $target_file)) {
                $renamedImage = $this->renameFile($target_dir, $fileName);

                $res = HotelImage::updateOrCreate(['hotel_id' => $hotel_id, 'type' => 'main'], [
                    'original_name' => $fileName,
                    'filename'      => $renamedImage,
                    'type'          => 'main'
                ]);

                if($res) {
                    return [
                        'status'        => 1,
                        'status_txt'    => trans('hotels.hotel_upload_mainphoto_success'),
                        'status_res'    => self::getMainPhoto( $hotel_id ),
                    ];
                } else {
                    return [
                        'status'        => -1,
                        'status_txt'    => trans('hotels.hotel_upload_mainphoto_error'),
                        'status_res'    => self::getMainPhoto( $hotel_id ),
                    ];
                }

            } else {
                return [
                    'status'         => -1,
                    'status_txt'    => trans('hotels.hotel_upload_mainphoto_error'),
                ];
                 $result;
            }

        } else {
            return [
                'status'         => -1,
                'status_txt'    => trans('hotels.hotel_upload_mainphoto_error'),
            ];
        }

    }

    public static function getMainPhoto( $hotel_id ){
        $file = '';
        $mainPhotoDir = self::getHotelImagesDirPath($hotel_id) . "main/";

        if( file_exists( $mainPhotoDir ) ) {
            $_files = scandir( $mainPhotoDir );

            foreach($_files as $_file)
            {
                if(is_file($mainPhotoDir.'/'.$_file)) {
                    $file = self::getHotelImagesDirUrl($hotel_id) . 'main/' . $_file;
                }
            }
        }

        return $file;
    }

    public function delMainPhoto(){

        if( $this->deleteMainPhoto($this->vars['hotel_id']) ){
            $affectedRows = HotelImage::where('hotel_id', '=', $this->vars['hotel_id'])->where('type', '=', 'main')->delete();

            if($affectedRows){
                return [
                    'status'         => 1,
                    'status_txt'    => trans('hotels.hotel_upload_mainphoto_del'),
                ];
            } else {
                //TODO:: Error Log
            }

        } else {
            return [
                'status'         => -1,
                'status_txt'    => trans('hotels.hotel_upload_mainphoto_del_e'),
            ];
        }
    }

    private function deleteMainPhoto( $hotel_id ){

        $mainPhotoDir = self::getHotelImagesDirPath($hotel_id) . "main/";

        if(!file_exists($mainPhotoDir)) return false;

        $files = scandir($mainPhotoDir);
        foreach($files as $file){
            if( is_file($mainPhotoDir.$file) ) unlink( $mainPhotoDir.$file );
        }

        return true;
    }

    public function uploadGallery()
    {
        $hotel_id = $this->vars['hotel_id'];

        $UploadHandler = new UploadHandler(array(
            'upload_dir'          => self::getHotelImagesDirPath($hotel_id),
            'upload_url'          => self::getHotelImagesDirUrl($hotel_id),
            'image_versions'    => array(
                'medium' => array(
                    'max_width' => 800,
                    'max_height' => 600
                ),
                'thumbnail' => array(
                    'max_width' => 200,
                    'max_height' => 200
                )
            ),
            'print_response'    => false,
        ));

        $response       = $UploadHandler->get_response();

        $bigImgDir      = self::getHotelImagesDirPath($hotel_id);
        $mediumImgDir   = self::getHotelImagesDirPath($hotel_id) . '/medium/';
        $smallImgDir    = self::getHotelImagesDirPath($hotel_id) . '/thumbnail/';

        $newFileName = $this->renameFile($bigImgDir, $response['files'][0]->name);
        $this->renameFile($mediumImgDir, $response['files'][0]->name);
        $this->renameFile($smallImgDir, $response['files'][0]->name);

        $res = HotelImage::updateOrCreate(['id' => 0], [
            'hotel_id'      => $hotel_id,
            'original_name' => $response['files'][0]->name,
            'filename'      => $newFileName,
            'type'          => 'gallery'
        ]);

        $res = ['files' =>[
                    0 =>
                        [
                            'name'          => $newFileName,
                            'size'          => $response['files'][0]->size,
                            'type'          => $response['files'][0]->type,
                            'deleteType'    => "post",
                            'url'           => self::getHotelImagesDirUrl($hotel_id) . $newFileName,
                            'mediumUrl'     => self::getHotelImagesDirUrl($hotel_id) . 'medium/' . $newFileName,
                            'thumbnailUrl'  => self::getHotelImagesDirUrl($hotel_id) . 'thumbnail/' . $newFileName,
                            'deleteUrl'     => url('/')."/index.php?file=" . $newFileName,
                    ]
                        ]
                ];

        return json_encode($res);
    }

    public function loadGallery(){

        $hotel_id = $this->vars['hotel_id'];

        $_images = self::getGalleryImages($hotel_id, 'thumbnail');
        $images = array();
        foreach ( $_images as $image ) {
            $images['files'][] = array (
                'name'          => '',
                'deleteType'    => "post",
                'size'          => 46353,
                'url'           => $image['url'],
                'thumbnailUrl'  => $image['url'],
                'deleteUrl'     => url('/')."/ajax?method=Files.deleteImage&formData[hotel_id]=".$hotel_id."&formData[filename]=".$image['filename']."&_token=" . csrf_token(),
            );
        }

        return json_encode($images);
    }

    public function deleteImage(){

        $filename = $this->vars['filename'];
        $dir = self::getHotelImagesDirPath( $this->vars['hotel_id'] );
        $this->recDeleteImages($dir, $filename);

        $affectedRows = HotelImage::where('hotel_id', '=', $this->vars['hotel_id'])->where('filename', '=', $filename)->delete();

        if(count($this->temp) == 3){
            $success['files'][$filename] = true;
            $this->temp = '';

            return json_encode($success);

        } else {
            //TODO: WRITE IN ERROR REPORT LOG
        }

    }

    public static function getGalleryImages($hotel_id, $imgSize = 'big'){

        $files = array();

        if($imgSize == 'big'){
            $galleryPAth = self::getHotelImagesDirPath($hotel_id);

        } elseif ( $imgSize == 'medium' ){
            $galleryPAth = self::getHotelImagesDirPath($hotel_id) . '/medium';
        } elseif ( $imgSize == 'thumbnail' ){
            $galleryPAth = self::getHotelImagesDirPath($hotel_id) . '/thumbnail';
        } else {
            $galleryPAth = self::getHotelImagesDirPath($hotel_id);
        }

        if( file_exists($galleryPAth) ) {
            $_files = scandir($galleryPAth);

            foreach($_files as $key => $file)
            {
                if(is_file($galleryPAth.'/'.$file)) {
                    if($imgSize == 'big'){
                        $files[$key]['url'] = self::getHotelImagesDirUrl($hotel_id)  . $file;
                        $files[$key]['filename'] = $file;
                    } else {
                        $files[$key]['url'] = self::getHotelImagesDirUrl($hotel_id) . $imgSize .'/'.$file;
                        $files[$key]['filename'] = $file;

                    }
                }
            }
        }

        return $files;

    }

    public static function getHotelImagesDirPath($hotel_id){
        return public_path().'/images/hotels/' . $hotel_id . '/';
    }

    public static function getHotelImagesDirUrl($hotel_id){
        return asset('/images/hotels/' . $hotel_id) . '/';
    }

    private function renameFile($filePath, $file){

            preg_match("/(.+)\.(.+)/", $file, $fileArr);
            $md5_name    = md5($fileArr[1]);
            $format      = $fileArr[2];
            $newFileName = $md5_name . '.' . $format;

            if (file_exists($filePath . $file)) {
                $res = rename($filePath . $file, $filePath . $newFileName);
            } else {
                $outputArr['files'][0]['name']  = $file;
                $outputArr['files'][0]['error'] = "Файлът не е намерен";

                return $outputArr;
            }

            if ($res) {

                return $newFileName;

            } else {
                $outputArr['files'][0]['name']  = $file;
                $outputArr['files'][0]['error'] = "Грешка в преименуването на файловете";

                return $outputArr;
            }

    }

    private function recDeleteImages($dir, $filename){
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if(is_file($dir.'/'.$object) && trim(strtolower($object)) == trim(strtolower($filename)) ){
                        if( unlink( $dir.'/'.$object ) ){
                            $this->temp[] = $dir.$object;
                        }
                    } else {
                        $this->recDeleteImages($dir.$object, $filename);
                    }
                }
            }
        }
    }


}