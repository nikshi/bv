<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 * Date: 1/27/17
 * Time: 11:12 PM
 */

namespace App\Services;

use Validator;

use Illuminate\Support\Facades\Auth as Auth;
use App\HotelModel as UserModel;

class UserService extends BaseService
{

    private $vars = array();

    function __construct($vars = [])
    {
        $this->vars = $vars;
    }


    public function changePassword(){

        $validator = Validator::make($this->vars, UserModel::$validation_rules );

        if ( $validator->fails() ) {
            return array( "status" => -1, "status_txt" => $validator->messages()->first() );
        } else{

            $user = User::find(auth()->user()->id);

            if(!Hash::check($this->vars['old_password'], $user->password)){
                return back()
                    ->with('error','The specified password does not match the database password');
            }else{
                return "all is ready for update";
            }

        }

//        return $result;
    }

}