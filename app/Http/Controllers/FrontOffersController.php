<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth as Auth;
use App\Services\HotelService as HotelService;
use App\Services\HotelFacilitiesService as HotelFacilitiesService;
use App\Services\RoomsService as RoomsService;

class FrontOffersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function init()
    {

        $variables = [

        ];

        return view('clients.offers', $variables);
    }

}
