<?php

namespace App\Http\Controllers;

use App\Events\StatisticsEvent;
use App\Services\BlogService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth as Auth;
use App\Services\HotelService as HotelService;
use App\Services\FilesService as FilesService;
use App\Services\HotelFacilitiesService as HotelFacilitiesService;
use App\Services\RoomsService as RoomsService;
use App\Services\ReviewsService as ReviewsService;

use App\Hotel;


class HotelsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function createEditHotel()
    {
        $user = Auth::user();
        $HotelService = new HotelService;
        $RoomsService = new RoomsService;
        $FilesService = new FilesService;



        $hotelDetails = [];
        $rooms = [];
        $hotelFacilities = [];
        $main_photo = '';
        //Temporary method... work only with one hotel by user
        $hotel = $HotelService->getHotelIdByUserId( $user->id );

        if( $hotel ){
            $hotelDetails       = $HotelService->getRawDetails($hotel->id);
            $rooms              = $RoomsService->getRawDetails($hotel->id);
            $hotelFacilities    = HotelFacilitiesService::getHotelFacilitiesIds($hotel->id);
            $main_photo         = FilesService::getMainPhoto( $hotel->id );
        }

        $variables = [
            'stars'                     => config('consts.stars'),
            'hotel_type'                => config('consts.hotel_type'),
            'locate_area'               => config('consts.locate_area'),
            'locate_municipalities'     => config('consts.locate_municipalities'),
            'hotel_facilities'          => config('consts.hotel_facilities'),
            'room_types'                => config('consts.room_types'),
            'room_facilities'           => config('consts.room_facilities'),
            'hotelDetails'              => $hotelDetails,
            'hotelFacilities'           => $hotelFacilities,
            'rooms'                     => $rooms,
            'hotel_main_photo'          => $main_photo
        ];



        return view('hosts.hotel', $variables);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function hotelsList(Request $request)
    {

        $params = $request->input('formData');

        $variables = [
            'stars'                     => config('consts.stars'),
            'hotel_type'                => config('consts.hotel_type'),
            'locate_area'               => config('consts.locate_area'),
            'locate_municipalities'     => config('consts.locate_municipalities'),
            'hotel_facilities'          => config('consts.hotel_facilities'),
            'room_types'                => config('consts.room_types'),
            'room_facilities'           => config('consts.room_facilities'),
            'hotels_ordering'           => config('consts.hotels_ordering'),
            'params'                    => $params
        ];

        return view('clients.hotels', $variables);
    }

    public function getHotel($slug){

        $HotelService       = new HotelService;
        $ReviewsServices    = new ReviewsService;

        $hotel_id       = $HotelService->getHotelIdBySlug($slug);
        $hotelDetails   = $HotelService->getHotelDetails($hotel_id->id);
        $reviews        = $ReviewsServices->getReviews($hotel_id->id);
        $ratings        = $ReviewsServices->getHotelRatings($hotel_id->id);

        $BlogService        = new BlogService();

        $destinations_posts = $BlogService->getBlogPosts(10);

        $variables = [
            'stars'                     => config('consts.stars'),
            'hotel_type'                => config('consts.hotel_type'),
            'locate_area'               => config('consts.locate_area'),
            'locate_municipalities'     => config('consts.locate_municipalities'),
            'hotel_facilities'          => config('consts.hotel_facilities'),
            'room_types'                => config('consts.room_types'),
            'room_facilities'           => config('consts.room_facilities'),
            'visiting_types'            => config('consts.visiting_types'),
            'hotelDetails'              => $hotelDetails,
            'reviews'                   => $reviews,
            'ratings'                   => $ratings,
            'destinations_posts'        => $destinations_posts,
            'randomHotels'              => $HotelService->getRandomHotels(5)
        ];

        event(new StatisticsEvent('viewHotel', $hotel_id->id));

        $test = Hotel::where('id', '=', 16)->firstOrFail();

//        echo "<pre>=========>>>>> " . print_r($test->images,1) . " <<<=================</pre>";

        return view('clients.hotelDetails', $variables);
    }

}
