<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth as Auth;
use App\Services\HotelService as HotelService;
use App\Services\HotelFacilitiesService as HotelFacilitiesService;
use App\Services\RoomsService as RoomsService;

class MapController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function init(Request $request)
    {

        $params = $request->input('formData');

        $variables = [
            'stars'                     => config('consts.stars'),
            'hotel_type'                => config('consts.hotel_type'),
            'locate_area'               => config('consts.locate_area'),
            'locate_municipalities'     => config('consts.locate_municipalities'),
            'hotel_facilities'          => config('consts.hotel_facilities'),
            'room_types'                => config('consts.room_types'),
            'room_facilities'           => config('consts.room_facilities'),
            'hotels_ordering'           => config('consts.hotels_ordering'),
            'params'                    => $params
        ];

        return view('clients.map', $variables);
    }

}
