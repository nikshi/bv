<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth as Auth;
use App\Services\HotelService as HotelService;
use App\Services\MessagesService as MessagesService;

class MessagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function init()
    {
        $user = Auth::user();
        $HotelService = new HotelService;
        $hotel = $HotelService->getHotelIdByUserId( $user->id );
        $messageService = new MessagesService;
        $messages = $messageService->getMessages( $hotel->id );

        $variables = [
            'messages'     => $messages,

        ];

        return view('hosts.messages', $variables);
    }


    public function chat( $token )
    {
        $user = Auth::user();
        $HotelService = new HotelService;
        $hotel = $HotelService->getHotelIdByUserId( $user->id );
        $messageService = new MessagesService;
        $messages = $messageService->getMessagesByToken($token);
        foreach ( $messages as $message ){
            $messageService->markAsRead($message);
        }
        $variables = [
            'messages'    => $messages,
            'user'        => $user,

        ];

        return view('hosts.messagesChat', $variables);
    }

}
