<?php

namespace App\Http\Controllers;

use App\Services\BlogService;
use App\Services\StatisticsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth as Auth;
use App\Services\HotelService as HotelService;

class HomepageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function init()
    {
        $topViewedWeek = [];
        $topSearchWeekHotels = [];

        $hotelService       = new HotelService();
        $StatisticsService  = new StatisticsService();
        $BlogService        = new BlogService();

        $reviews_posts      = $BlogService->getBlogPosts(2);
        $destinations_posts = $BlogService->getBlogPosts(10, 8);

        $_topViewedWeek = $StatisticsService->getTopViewedWeekHotels(5);
        $_TopSearchWeekHotels = $StatisticsService->getTopSearchWeekHotels(5);

        foreach ($_topViewedWeek as $hotel) {
            $topViewedWeek[] = $hotelService->getHotelDetails($hotel->hotel_id);
        }

        foreach ($_TopSearchWeekHotels as $hotel) {
            $topSearchWeekHotels[] = $hotelService->getHotelDetails($hotel->hotel_id);
        }

        $variables = [
            'featuredHotels'    => $hotelService->getFeaturedHotels(),
            'randomHotels'      => $hotelService->getRandomHotels(4),
            'seaHotels'         => $hotelService->getSeaHotels(4),
            'price10Hotels'     => $hotelService->getHotelsByMaxPrice(10, 4),
            'topViewedWeek'     => $topViewedWeek,
            'topSearchWeek'     => $topSearchWeekHotels,
            'reviews_posts'     => $reviews_posts,
            'destinations_posts'=> $destinations_posts,
        ];

        return view('clients.homepage', $variables);
    }

}
