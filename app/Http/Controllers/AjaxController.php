<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\HotelService;

class AjaxController extends Controller
{

    /**
     * Catch Ajax request and find correct service
     * @param Request $request
     *
     * @return array
     */
    public function event(Request $request)
    {

        $rezult        = array();
        $taskFull      = $request->input('method');
        $task          = explode(".", $taskFull );
        $this->Service = $task[0];
        $this->method  = $task[1];


        if (!isset($this->Service)) {
            $rezult['status']     = -2;
            $rezult['status_txt'] = "Error in data";

            return $rezult;

        } else {
            $Service = 'App\Services\\' . $this->Service . 'Service';
        }

        if (!isset($this->method)) {
            $rezult['status']     = -3;
            $rezult['status_txt'] = "Error in data 2";

            return $rezult;

        } else $method = $this->method;

        switch ($this->method) {
            default:
                if (method_exists($Service, $method)) {
                    $params = $request->input('formData');
                    $intClass = new  $Service($params);
                    $rezult   = $intClass->$method( $request );
                } else {
                    $rezult['status']       = -1;
                    $rezult['status_title'] = "Системно съобщение";
                    $rezult['status_txt']   = "Грешка.Липсващ метод " . $Service . "." . $method;
                }

        } // switch

        return $rezult;

    }
}
