<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth as Auth;
use App\Services\HotelService as HotelService;
use App\Services\HotelFacilitiesService as HotelFacilitiesService;
use App\Services\RoomsService as RoomsService;

class HotelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function init()
    {
        $user = Auth::user();
        $HotelService = new HotelService;
        $RoomsService = new RoomsService;

        $hotelDetails = [];
        $rooms = [];
        $hotelFacilities = [];

        //Temporary method... work only with one hotel by user
        $hotel = $HotelService->getHotelIdByUserId( $user->id );

        if( $hotel ){
            $hotelDetails       = $HotelService->getRawDetails($hotel->id);
            $rooms              = $RoomsService->getRawDetails($hotel->id);
            $hotelFacilities    = HotelFacilitiesService::getHotelFacilitiesIds($hotel->id);
        }

        $variables = [
            'stars'                     => config('consts.stars'),
            'hotel_type'                => config('consts.hotel_type'),
            'locate_area'               => config('consts.locate_area'),
            'locate_municipalities'     => config('consts.locate_municipalities'),
            'hotel_facilities'          => config('consts.hotel_facilities'),
            'room_types'                => config('consts.room_types'),
            'room_facilities'           => config('consts.room_facilities'),
            'hotelDetails'              => $hotelDetails,
            'hotelFacilities'           => $hotelFacilities,
            'rooms'                     => $rooms
        ];



        return view('hosts.hotel', $variables);
    }

}
