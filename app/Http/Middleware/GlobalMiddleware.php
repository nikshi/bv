<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Cookie;
class GlobalMiddleware
{

    /**
     * Handle an incoming request.
     */
    public function handle(Request $request, Closure $next, $guard = null)
    {

//        Cookie::queue('guest', [''], 2);

//        $data = $request->cookies->has('visitor');
//        echo '<pre><br>=====DEBUG START======<br> '.print_r($data,1).' <br>=====DEBUG END======<br></pre>';

        return $next($request);
    }
}
