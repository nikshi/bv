<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Hotels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status');
            $table->string('name');
            $table->string('slug');
            $table->text('description');
            $table->integer('hotel_type');
            $table->integer('stars');
            $table->integer('owner');
            $table->string('contact_name');
            $table->string('phone1');
            $table->string('phone2');
            $table->string('email');
            $table->string('website');
            $table->integer('area');
            $table->integer('municipality');
            $table->string('city');
            $table->string('address');
            $table->integer('distance_sea');
            $table->integer('distance_ski');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}
