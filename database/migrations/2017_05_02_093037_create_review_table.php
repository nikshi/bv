<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hotel_id');
            $table->integer('user_id');
            $table->string('user_email');
            $table->string('user_name');
            $table->string('review_title');
            $table->text('review_comment');
            $table->integer('visiting_type');
            $table->integer('service');
            $table->integer('food');
            $table->integer('relax');
            $table->integer('location');
            $table->integer('price');
            $table->integer('clean');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
