/**
 * Created by phoenix on 3/18/17.
 */
var map;
jQuery( document ).ready(function($){


    jQuery(document).on('click', '#write_review_btn', function(){

        if( jQuery(this).hasClass('opened') ){
            jQuery('#hotel-write-review' ).stop( true,true ).slideUp(200);
            jQuery(this).removeClass('opened');
        } else {
            jQuery( '#hotel-write-review' ).stop( true,true ).slideDown(200);
            jQuery(this).addClass('opened');
        }


    });
    // (
    //     function() {
    //         $( '#user_menu ul' ).stop( true,true ).slideDown(200)
    //     }, function() {
    //         $( '#user_menu ul' ).stop( true,true ).slideUp(200)
    //     }
    // );


    jQuery(document).on('click', '#visiting_types_btns li', function (e) {
       var type = jQuery(this).data('type');
       jQuery('#visiting_type').val(type);
    })

    jQuery('#googleMapTap').on('shown.bs.tab', function (e) {
        initMap();
        google.maps.event.trigger(map, 'resize');

    });

    jQuery(document).on('click', '#sendToHotelMsg', function (e) {
        e.preventDefault();

        var formData = jQuery(this).closest('form').serialize();

        jQuery.ajax({
            url: urls.ajax,
            data: formData,
            method: 'POST',
            error: function(data) {
            },
            dataType: 'json',
            success: function(data) {
                if(data.status > 0){
                    toastr.success(data.status_txt);

                    jQuery('[name="formData[name]"]').val('');
                    jQuery('[name="formData[email]"]').val('');
                    jQuery('[name="formData[phone]"]').val('');
                    jQuery('[name="formData[message]"]').val('');

                } else {
                    toastr.error(data.status_txt);

                }

            },
        }).always(function() {
            // HoldOn.close();
        });

    })




});


function initMap() {

    var lat = parseFloat(jQuery('#hotel_lat').val());
    var lng = parseFloat(jQuery('#hotel_lng').val());

    var latlng = new google.maps.LatLng(lat, lng);

    map = new google.maps.Map(document.getElementById('hotelMap'), {
        zoom: 14,
        center: latlng
    });

    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
    });
}




// Faceboom login


jQuery(document).on('click', '#sendReview', function (e) {
    e.preventDefault();
    _login();
})

jQuery(document).on('click', '#status .logout', function (e) {
    _logout();
})

function writeReview(fb) {


    var formData = jQuery('#reviews_form').serialize();
    formData = formData.concat('&formData[user_id]='+fb.id);
    formData = formData.concat('&formData[user_email]='+fb.email);
    formData = formData.concat('&formData[user_name]='+fb.name);

    jQuery.ajax({
        url: urls.ajax,
        data: formData,
        method: 'POST',
        error: function(data) {
        },
        dataType: 'json',
        success: function(data) {
            if(data.status > 0){
                toastr.success(data.status_txt);

            } else {
                toastr.error(data.status_txt);
            }

        },
    }).always(function() {
        // HoldOn.close();
    });

}

function _login() {
    FB.login(function(response) {
        // handle the response
        if(response.status==='connected') {
            loginUser();
        }
    }, {scope: 'public_profile,email'});
}

// function _logout() {
//     FB.logout(function(response) {
//         console.log(response);
//         document.getElementById('status').innerHTML =
//             'Все още не сте индефициран';
//     });
// }


// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
    // console.log('statusChangeCallback');
    // console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        // loginUser();
        FB.api('/me', {locale: 'bg_BG', fields: 'name, email,birthday, hometown,education,gender,website,work'}, function(response) {
            document.getElementById('status').innerHTML =
                'Ревюто ще бъде публикувано от:<br> ' + response.name;
        });
    } else {
        // The person is not logged into your app or we are unable to tell.
        document.getElementById('status').innerHTML = 'Не сте индефицирани във Facebook';
    }
}

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}

// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function loginUser() {
    FB.api('/me', {locale: 'bg_BG', fields: 'name, email,birthday, hometown,education,gender,website,work'}, function(response) {
        writeReview(response);
        document.getElementById('status').innerHTML =
            'Ревюто ще бъде публикувано от:<br> ' + response.name;
    });

}