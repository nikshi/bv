/**
 * Created by phoenix on 2/11/17.
 */


jQuery(document).ready(function(){

    dashboardHotelMap();

});


jQuery.uploadPreview({
    input_field: "#image-upload",   // Default: .image-upload
    preview_box: "#image-preview",  // Default: .image-preview
    label_field: "#image-label",    // Default: .image-label
    label_default: "Избери снимка",   // Default: Choose File
    label_selected: "Избери снимка",  // Default: Change File
    no_label: false                 // Default: false
});




// Google map in Dashboard hotel page
function dashboardHotelMap() {

    var map = null;


    if(document.getElementById('hotel_lat').value > 0 && document.getElementById('hotel_lng').value > 0 ){
        latLng  = {
            lat: parseFloat(document.getElementById('hotel_lat').value),
            lng: parseFloat(document.getElementById('hotel_lng').value)
        };
        map = new google.maps.Map(document.getElementById('dashboardHotelMap'), {
            zoom: 16,
            center: latLng
        });

        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            draggable:true
        });
    } else {

        var latLng = {lat: 42.561941, lng: 25.6321389};

        map = new google.maps.Map(document.getElementById('dashboardHotelMap'), {
            zoom: 7,
            center: latLng
        });

        var marker = new google.maps.Marker({
            map: map,
            draggable:true
        });
    }


    var geocoder = new google.maps.Geocoder();

    document.getElementById('showMeOnMap').addEventListener('click', function(e) {
        e.preventDefault();
        map.setZoom(16);
        geocodeAddress(geocoder, map);
    });

    function geocodeAddress(geocoder, resultsMap) {

        var locate_areaEl           = document.getElementById('locate_area');
        var locate_area             = locate_areaEl.options[locate_areaEl.selectedIndex].text;
        var locate_city             = document.getElementById('city').value;
        var locate_address          = document.getElementById('address').value;
        var locate_country          = 'България';

        var address = locate_address  + ", " + locate_city + ", " + locate_area+ ", " + locate_country;

        geocoder.geocode({'address': address}, function(results, status) {
            if (status === 'OK') {
                resultsMap.setCenter(results[0].geometry.location);
                marker.setPosition(results[0].geometry.location);
                document.getElementById('hotel_lat').value = marker.getPosition().lat().toFixed(5);
                document.getElementById('hotel_lng').value = marker.getPosition().lng().toFixed(5);
            } else {
                var address_alt = locate_city + ", " + locate_area+ ", " + locate_country;
                geocoder.geocode({'address': address_alt}, function(results, status) {
                    if (status === 'OK') {
                        resultsMap.setCenter(results[0].geometry.location);
                        marker.setPosition(results[0].geometry.location);
                        document.getElementById('hotel_lat').value = marker.getPosition().lat().toFixed(5);
                        document.getElementById('hotel_lng').value = marker.getPosition().lng().toFixed(5);
                    } else {
                        alert('Населеното място не е намерено');
                    }
                });

            }
        });
    }

    marker.addListener('dragend', function (evt) {
        document.getElementById('hotel_lat').value = evt.latLng.lat().toFixed(5);
        document.getElementById('hotel_lng').value = evt.latLng.lng().toFixed(5);
    });

}




