/**
 * Created by phoenix on 2/27/17.
 */

jQuery(document).ready(function(){

    jQuery(document).on('click', '#filterSearchBtn', function (e) {
        e.preventDefault();
        searchHotels();
    })

    jQuery(document).on('click', '#pagination li a', function (e) {
        e.preventDefault();
        var page = jQuery(this).data('href');
        if(page > 0) {
            jQuery('#hotelsFilterForm #page').val(page);
        }
        searchHotels();

    })

    jQuery(document).on('change', '#hotels_ordering', function (e) {
        e.preventDefault();
        var hotels_ordering = jQuery(this).val();
        console.log(hotels_ordering);
        jQuery('#hotels_ordering').val(hotels_ordering);
        searchHotels();

    })

    searchHotels();

    function searchHotels() {
        var formData = jQuery('#hotelsFilterForm').serialize();
        history.pushState(null, null, '?' + formData);
        ajax(formData);
    }


    function ajax(params){
        HoldOn.open();
        jQuery.ajax({
            url: urls.ajax,
            data: params,
            method: 'POST',
            error: function(data) {
            },
            dataType: 'json',
            success: function(data) {
                if(data.status > 0){
                    jQuery('#num_found_results').text('');
                    jQuery('.hotel-list').html('');

                    if( data.count == 1 ){
                        jQuery('#num_found_results').text(data.count + ' обект');
                    }else {
                        jQuery('#num_found_results').text(data.count + ' обекта');
                    }
                    jQuery('.hotel-list').append(data.data);
                }
            }
        }).always(function() {
            HoldOn.close();
        });
    }


});