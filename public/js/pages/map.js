/**
 * Created by phoenix on 2/27/17.
 */

jQuery(document).ready(function(){

    function hotels_map() {
        var lat = 41.5267;
        var lng = 26.12427;

        latlng = new google.maps.LatLng(lat, lng);
        infowindow = new google.maps.InfoWindow();
        markers = [];
        map = new google.maps.Map(document.getElementById('map_wrap'), {
            zoom: 14,
            center: latlng,
            scrollwheel: false,

        });
    }
    hotels_map();

    jQuery(document).on('click', '#filterSearchBtn', function (e) {
        e.preventDefault();
        searchHotels();
    });

    jQuery(document).on('click', '#pagination li a', function (e) {
        e.preventDefault();
        var page = jQuery(this).data('href');
        if(page > 0) {
            jQuery('#hotelsFilterForm #page').val(page);
        }
        searchHotels();
    });

    jQuery(document).on('click', '#hotelsFilterForm .panel', function () {
        setTimeout(function () {
            google.maps.event.trigger(map, 'resize')
        }, 250);
    });

    jQuery(document).on('change', '#hotels_ordering', function (e) {
        e.preventDefault();
        var hotels_ordering = jQuery(this).val();
        console.log(hotels_ordering);
        jQuery('#hotels_ordering').val(hotels_ordering);
        searchHotels();

    })

    searchHotels();

    function searchHotels() {
        var formData = jQuery('#hotelsFilterForm').serialize();
        history.pushState(null, null, '?' + formData);
        ajax(formData);
    }


    function ajax(params){
        HoldOn.open();
        jQuery.ajax({
            url: urls.ajax,
            data: params,
            method: 'POST',
            error: function(data) {
            },
            dataType: 'json',
            success: function(data) {
                if(data.status > 0){
                    jQuery('#num_found_results').text('');
                    jQuery('.hotel-list').html('');

                    if( data.count == 1 ){
                        jQuery('#num_found_results').text(data.count + ' обект');
                    }else {
                        jQuery('#num_found_results').text(data.count + ' обекта');
                    }
                    setMapOnAll(null);
                    markers = [];
                    bounds = new google.maps.LatLngBounds();
                    jQuery.each(data.data, function (i, hotel) {
                        var latLng = new google.maps.LatLng(hotel.hotel_lat, hotel.hotel_lng);
                        var marker = new google.maps.Marker({
                            position: latLng,
                            map: map,
                        });
                        markers.push(marker);
                        google.maps.event.addListener(marker, 'click', function(){
                            infowindow.close(); // Close previously opened infowindow
                            infowindow.setContent( "<div class='infowindow'><div class='col-md-4 image'><img width='100%' src='"+hotel.image+"' alt='"+hotel.name+"'> </div><div class='col-md-8 text'><h4><a href='"+hotel.link+"'>"+hotel.name+"</a></h4><p>"+hotel.description.substring(0,100)+"</p></div></div>");
                            infowindow.open(map, marker);
                        });
                        bounds.extend(latLng);
                    });


                    map.fitBounds(bounds);


                    // setMapOnAll(map);
                    var lat = 41.5267;
                    var lng = 26.12427;

                    latlng = new google.maps.LatLng(lat, lng);
                    map.panBy(-140,0);

                }
            }
        }).always(function() {
            HoldOn.close();
        });
    }


    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }


});