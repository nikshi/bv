/**
 * Created by phoenix on 12/17/16.
 */

jQuery(document).ready(function(){

    jQuery('.search-select').select2();

    jQuery(".slider_ski").slider({
        min: 0,
        max: 10,
        step:0.5,
        change: function (event, ui) {
            jQuery('#distance_ski').val(ui.value);
        },
        values: [jQuery('#distance_ski').val()]
        }).slider("pips", {
            rest: "label",
            step: 2
        }).slider("float",{
        suffix: 'km'
    });

    jQuery(".slider_beach").slider({
        min: 0,
        max: 1000,
        step: 50,
        change: function (event, ui) {
            jQuery('#distance_sea').val(ui.value);
        },
        values: [jQuery('#distance_sea').val()]
        }).slider("pips", {
            rest: "label",
            step: 5,
        }).slider("float", {
        suffix: 'm'
    });

    jQuery(".price_range").slider({
        min: 0,
        max: 300,
        step: 10,
        range: true,
        change: function (event, ui) {
            jQuery('#price_min').val(ui.values[0]);
            jQuery('#price_max').val(ui.values[1]);
        },
        values: [jQuery('#price_min').val(), jQuery('#price_max').val()]
    }).slider("pips", {
        rest: "label",
        step: 5
    }).slider("float", {
        suffix: 'лв.'
    });


    // jQuery('.multi-checkbox').multiselect( 'settings', { columns: 2 });

    jQuery('select[multiple].multi-checkbox').multiselect({
        columns: 3,
        search: true,
        texts: {
            placeholder: '',
            search: 'Търсене',
            selectedOptions:' избрани'
        }
    });

    getRooms();

    jQuery(document).on('click', '#btnEditRoom', function () {
        $room_values = jQuery(this).closest('.room').find('.room_value');

        jQuery.each($room_values, function (i, field) {

            var name = jQuery(field).data('name');
            var fined_field = jQuery('#hotelRooms').find( '*[data-field="'+name+'"]' );

            if( fined_field.length > 0 ){

                if( fined_field.getType() == 'text' ||
                    fined_field.getType() == 'select' ||
                    fined_field.getType() == 'textarea' ||
                    (fined_field.getType() == 'hidden' )
                ){
                    if(jQuery(fined_field).data('facility')) return true;
                    jQuery(fined_field).val(jQuery(field).data('value'));
                }
                if(fined_field.getType() == 'checkbox') {
                    var checked = jQuery(field).data('value');
                    jQuery.each(fined_field, function (i, f) {

                        if(jQuery(f).attr('name') == 'formData[facilities]['+checked+']' ){
                            jQuery(f).prop( "checked", true);
                        }
                    })
                }

            }

        })
    });


});


jQuery.fn.getType = function(){ return this[0].tagName == "INPUT" ? this[0].type.toLowerCase() : this[0].tagName.toLowerCase(); }
