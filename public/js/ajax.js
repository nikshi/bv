/**
 * Created by phoenix on 2/1/17.
 */

jQuery(document).ready(function(){


    // Event Handlers

    jQuery(document).on('click', '#uploadMainPhotoBtn', function(е){
        HoldOn.open();
        е.preventDefault();
        uploadMainImage();
    });

    jQuery(document).on('click', '#deleteMainPhotoBtn', function(е){
        е.preventDefault();
        save(jQuery('#deleteMainPhoto').serialize(), removeMainPhoto());
    });

    jQuery(document).on('click', '#hotelSaveBtn', function(е){
        е.preventDefault();
        save(jQuery('#hotelForm').serialize());
    });

    jQuery(document).on('click', '#userSaveBtn', function(e){
        e.preventDefault();
        save( jQuery('#userForm').serialize() );
    });

    jQuery(document).on('click', '#roomSaveBtn', function(e){
        e.preventDefault();
        save( jQuery('#hotelRooms').serialize() );
    });

    jQuery(document).on('click', '#btnDelRoom', function(e){
        e.preventDefault();
        var room_id = jQuery(this).data('room-id');
        deleteRoom(room_id);
    });

    jQuery(document).on('click', '#sendReplyMsg', function(e){
        e.preventDefault();
        save(jQuery('#msgreply').serialize());
        location.reload();
    });

    // CallBack Functions

    function removeMainPhoto(){
        jQuery('#image-preview').css({'background' : '#fff'});
    }


    // Ajax Functions

    function uploadMainImage() {
        $.ajax( {
            url: urls.ajax,
            type: 'POST',
            data: new FormData( jQuery('#uploadMainPhoto')[0] ),
            processData: false,
            contentType: false,
            success: function (data) {
                if(data.status > 0){
                    toastr.success(data.status_txt)
                } else {
                    toastr.error(data.status_txt)
                }
            }
        } )
            .always(function() {
                HoldOn.close();
            });
    }

    function save(params){
        HoldOn.open();
        jQuery.ajax({
                url: urls.ajax,
                data: params,
                method: 'POST',
                error: function(data) {
                },
                dataType: 'json',
                success: function(data) {
                    getRooms();
                    if(data.status > 0){

                        if( data.hotel_id > 0 ){
                            jQuery('.no-hotel').remove();
                            jQuery('.hotel_id').val(data.hotel_id);
                            jQuery(".gallery-wrap").removeClass('hidden')
                            jQuery(".room-wrap").removeClass('hidden')
                        }
                        toastr.success(data.status_txt)
                    } else {
                        toastr.error(data.status_txt)
                    }

                },
        }).always(function() {
            HoldOn.close();
        });
    }

});

function getRooms(){
    var hotel_id = jQuery('#hotelRooms').find('input[name="formData[hotel_id]"]');
    if( jQuery(hotel_id).val() > 0 ){
        $.ajax({
            url: urls.ajax,
            data: {
                '_token'                : jQuery('meta[name="csrf-token"]').attr('content'),
                'method'                : 'Rooms.getRoomsHtml',
                'formData'    : {
                    'hotel_id': jQuery(hotel_id).val()
                }
            },
            method: 'POST',
            dataType: 'html',
            error: function(data) {
                error.success("Сървърна грешка. Моля опитайте пак")
            },
            success: function(data) {
                jQuery('.rooms-list').empty();
                jQuery('.rooms-list').html(data);
            }
        });
    }
}


function deleteRoom(room_id){
        HoldOn.open();
        $.ajax({
            url: urls.ajax,
            data: {
                '_token'                : jQuery('meta[name="csrf-token"]').attr('content'),
                'method'                : 'Rooms.delete',
                'formData'    : {
                    'room_id': room_id
                }
            },
            method: 'POST',
            dataType: 'json',
            error: function(data) {
                error.success("Сървърна грешка. Моля опитайте пак")
            },
            success: function(data) {
                if(data.status > 0){
                    getRooms();
                    toastr.success(data.status_txt)
                } else {
                    toastr.error(data.status_txt)
                }
            }
        }).always(function() {
            HoldOn.close();
        });
}