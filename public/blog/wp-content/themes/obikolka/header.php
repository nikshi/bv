<?php
/* *
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package sparkling
 */

if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)) header('X-UA-Compatible: IE=edge,chrome=1'); ?>
<!doctype html>
<!--[if !IE]>
<html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>
<html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>
<html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>
<html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="theme-color" content="<?php echo of_get_option( 'nav_bg_color' ); ?>">
<link rel="profile" href="http://gmpg.org/xfn/11">



<?php wp_head(); ?>
<!-- Main Style -->
<link id="main-style" rel="stylesheet" href="https://obikolka.com/template/css/style.css">
<!-- Custom Styles -->
<link rel="stylesheet" href="https://obikolka.com/css/custom.css">

</head>

<body <?php body_class(); ?>>

<header id="header" class="navbar-static-top">

    <div class="topnav hidden-xs">
        <div class="container">
            <ul class="quick-menu pull-right">
                <li><a href="https://obikolka.com/login">Вход</a></li>
                <li><a href="https://obikolka.com/register">Регистрация</a></li>
            </ul>
        </div>
    </div>

    <div class="main-header">

        <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">
            Mobile Menu Toggle
        </a>

        <div class="container">
            <h1 class="logo navbar-brand">
                <a href="https://obikolka.com" title="">
                    <img src="https://obikolka.com/template/images/logo.png" alt="Logo">
                </a>
            </h1>
            <nav id="main-menu" role="navigation">
			    <ul class="menu">
			        <li class="menu-item-has-children">
			            <a href="https://obikolka.com">Начало</a>
			        </li>
			        <li class="menu-item-has-children">
			            <a href="https://obikolka.com/hotels">Хотели</a>
			        </li>
			        <li class="menu-item-has-children">
			            <a href="https://obikolka.com/offers">Оферти</a>
			        </li>
			        <li class="menu-item-has-children">
			            <a href="https://obikolka.com/map">Карта</a>
			        </li>
			        <li class="menu-item-has-children">
			            <a href="https://obikolka.com/blog">Блог</a>
			        </li>
			    </ul>
			</nav>
        </div>

        <nav id="mobile-menu-01" class="mobile-menu collapse">
		    <ul id="mobile-primary-menu" class="menu">
		        <li class="menu-item-has-children">
		            <a href="{{url('/')}}">{{ trans('header.home') }}</a><button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-0"></button>
		        </li>
		        <li class="menu-item-has-children">
		            <a href="{{url('/hotels')}}">{{ trans('header.hotels') }}</a><button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-1"></button>
		        </li>
		        <li class="menu-item-has-children">
		            <a href="{{url('/offers')}}">{{ trans('header.offers') }}</a><button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-2"></button>
		        </li>
		        <li class="menu-item-has-children">
		            <a href="{{url('/map')}}">{{ trans('header.map') }}</a><button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-3"></button>
		        </li>
		    </ul>

		    <ul class="mobile-topnav container">
		        <li><a href="#">MY ACCOUNT</a></li>
		        <li><a href="#travelo-login" class="soap-popupbox">LOGIN</a></li>
		        <li><a href="#travelo-signup" class="soap-popupbox">SIGNUP</a></li>
		    </ul>

		</nav>    
	</div>

</header>


<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Туристически блог</h2>
        </div>
        <ul class="breadcrumbs pull-right">
                <li><a href="#" class="active">Начало</a></li>
        </ul>
    </div>
</div>

<div id="page" class="hfeed site">

	<div id="content" class="site-content">

		<div class="top-section">
			<?php sparkling_featured_slider(); ?>
			<?php sparkling_call_for_action(); ?>
		</div>

		<div class="container main-content-area">
            <?php $layout_class = get_layout_class(); ?>
			<div class="row <?php echo $layout_class; ?>">
				<div class="main-content-inner <?php echo sparkling_main_content_bootstrap_classes(); ?>">
