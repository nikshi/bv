<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'nikshi_bv_blog');

/** MySQL database username */
define('DB_USER', 'nikshi_bv_blog');

/** MySQL database password */
define('DB_PASSWORD', 'n880513');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Qk4SjOYKu?Ji*K?->%j]D_VYzq^fxt4~NY/wpmj=5vy<9*7BPzq?M8gO#QD!FI1Q');
define('SECURE_AUTH_KEY',  ':D2s#Y;eP?+N>|w7ZGRYW4Djk|%k3{>3-8O~epk.KFufyCQL)[Z*J|0UF5.AT3<8');
define('LOGGED_IN_KEY',    'NI+GJ$tjpyPiOOCKWyE8[PUEhzSSe1XivXo;FoWC}^W)0;zB=#bzaq^Zj>lc_ut`');
define('NONCE_KEY',        'tT]&!Zn=B^]{!7KwFjG$^HS(;kPDHG}k*<aS,el]~L:f0NGCv43b[Va82+=?yXgl');
define('AUTH_SALT',        '=M^2]dr K6mse>L>gUI0[h{Javl5e%B]^Tn:3%:d!:mj1`.^M?Ds#ECoVb!)@Cdx');
define('SECURE_AUTH_SALT', 'NtzC/Q6l}0~CW*+=m_E~4Z0~9fQO0{|c_l bV;N}K5Xlidjefc=$8=(dsils`Uap');
define('LOGGED_IN_SALT',   '8JwE-{iuKpBGahR*L@K2S(dl.0{]Z85+:Bq.MG`$@u<Oz!5N<{a`hGC}v+8L-Fd0');
define('NONCE_SALT',       '|(v8cd$v+:`CV9:P,3).g1S!5{1A!J/T4`N 6]r2^^nTCL2Ig<o0P7knO95[5&8C');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
