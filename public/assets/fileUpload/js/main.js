/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */
jQuery(function ($) {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        url: urls.ajax,
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator && navigator.userAgent),
    });


    // Load existing files:
    $('#fileupload').addClass('fileupload-processing');
    var hotel_id = $('#fileupload').find('input[name="formData[hotel_id]"]').val();
    if( hotel_id > 0) {
        $.ajax({
            url: urls.ajax,
            method: 'post',
            dataType: 'json',
            context: $('#fileupload')[0],
            data: {
                _token: $('#fileupload').find('input[name="_token"]').val(),
                method: 'Files.loadGallery',
                formData: {
                    'hotel_id': hotel_id
                }
            }
        }).always(function () {
            $(this).removeClass('fileupload-processing');
        }).done(function (result) {
            $(this).fileupload('option', 'done').call(this, $.Event('done'), {result: result});
        });
    } else {
        $('.fileupload-processing .fileupload-process').removeClass('fileupload-processing');
    }

});
