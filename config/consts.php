<?php
/**
 * Created by PhpStorm.
 * User: phoenix
 * Date: 1/8/17
 * Time: 3:50 PM
 */

return [


    'msg_statuses' => [
        0 => 'Ново',
        1 => 'Прочетено'
    ],

    'hotels_ordering' => [
        'name_asc'      => "Име (А-Я)",
        'name_desc'     => "Име (Я-А)",
        'price_asc'     => "Цена (най-евтините първо)",
        'price_desc'    => "Цена (най-скъпите първо)",
//        'rating_desc'   => "Рейтинг (с най-висок първо)",
//        'rating_asc'    => "Рейтинг (с най-нисък първо)",
//        'views_desc'    => "Разглеждания (най-разглежданите първо)",
//        'views_asc'     => "Разглеждания (най-малко разглежданите първо)",
    ],

    'locate_area' => [
        ''=> 'Изберете област',
        1 => 'Благоевград',
        3 => 'Бургас',
        4 => 'Варна',
        5 => 'Велико Търново',
        6 => 'Видин',
        7 => 'Враца',
        8 => 'Габрово',
        9 => 'Добрич',
        10 => 'Кърджали',
        11 => 'Кюстендил',
        12 => 'Ловеч',
        13 => 'Монтана',
        14 => 'Пазарджик',
        15 => 'Перник',
        16 => 'Плевен',
        17 => 'Пловдив',
        18 => 'Разград',
        19 => 'Русе',
        20 => 'Силистра',
        21 => 'Сливен',
        22 => 'София',
        23 => 'Стара Загора',
        24 => 'Смолян',
        25 => 'Търговище',
        26 => 'Хасково',
        27 => 'Шумен',
        28 => 'Ямбол'
    ],

    'municipalities' => [
        1 => [
            1 => 'Банско',
            2 => 'Белица',
            3 => 'Благоевград',
            4 => 'Гоце Делчев',
            5 => 'Гърмен',
            6 => 'Кресна',
            7 => 'Петрич',
            8 => 'Разлог',
            9 => 'Сандански',
            10 => 'Сатовча',
            11 => 'Симитли',
            12 => 'Струмяни',
            13 => 'Хаджидимово',
            14 => 'Якоруда',
            15 => 'Оряхово'
        ],
        2 => [
            1 => 'Айтос',
            2 => 'Бургас',
            3 => 'Камено',
            4 => 'Карнобат',
            5 => 'Малко Търново',
            6 => 'Несебър',
            7 => 'Поморие',
            8 => 'Приморско',
            9 => 'Руен',
            10 => 'Созопол',
            11 => 'Средец',
            12 => 'Сунгурларе',
            13 => 'Царево',
            14 => 'Каварна'
        ],
        3 => [
            1 => 'Аврен',
            2 => 'Аксаково',
            3 => 'Белослав',
            4 => 'Бяла',
            5 => 'Варна',
            6 => 'Ветрино',
            7 => 'Вълчи дол',
            8 => 'Девня',
            9 => 'Долни чифлик',
            10 => 'Дългопол',
            11 => 'Провадия',
            12 => 'Суворово'
        ],
        4 => [
            1 => 'Велико Търново',
            2 => 'Горна Оряховица',
            3 => 'Елена',
            4 => 'Златарица',
            5 => 'Лясковец',
            6 => 'Павликени',
            7 => 'Полски Тръмбеш',
            8 => 'Свищов',
            9 => 'Стражица',
            10 => 'Сухиндол',
            11 => 'Априлци'
        ],
        5 => [
            1 => 'Белоградчик',
            2 => 'Бойница',
            3 => 'Брегово',
            4 => 'Видин',
            5 => 'Грамада',
            6 => 'Димово',
            7 => 'Кула',
            8 => 'Макреш',
            9 => 'Ново село',
            10 => 'Ружинци',
            11 => 'Чупрене',
        ],
        6 => [
            1 => 'Борован',
            2 => 'Бяла Слатина',
            3 => 'Враца',
            4 => 'Козлодуй',
            5 => 'Криводол',
            6 => 'Мездра',
            7 => 'Мизия',
            8 => 'Оряхово',
            9 => 'Роман',
            10 => 'Хайредин'
        ],
        7 => [
            1 => 'Габрово',
            2 => 'Дряново',
            3 => 'Севлиево',
            4 => 'Трявна'
        ],
        8 => [
            1=>	 'Балчик',
            2=>	 'Генерал Тошево',
            3=>	 'Добрич',
            4=>	 'Добрич-селска',
            5=>	 'Каварна',
            6=>	 'Крушари',
            7=>	 'Тервел',
            8=>	 'Шабла'
        ],
        9 => [
            1 => 'Ардино',
            2 => 'Джебел',
            3 => 'Кирково',
            4 => 'Крумовград',
            5 => 'Кърджали',
            6 => 'Момчилград',
            7 => 'Черноочене'
        ],
        10 => [
            1 => 'Бобов дол',
            2 => 'Бобошево',
            3 => 'Дупница',
            4 => 'Кочериново',
            5 => 'Кюстендил',
            6 => 'Невестино',
            7 => 'Рила',
            8 => 'Сапарева баня',
            9 => 'Трекляно'
        ],
        11 => [
            1 => 'Априлци',
            2 => 'Летница',
            3 => 'Ловеч',
            4 => 'Луковит',
            5 => 'Тетевен',
            6 => 'Троян',
            7 => 'Угърчин',
            8 => 'Ябланица'
        ],
        12 => [
            1 => 'Берковица',
            2 => 'Бойчиновци',
            3 => 'Брусарци',
            4 => 'Вълчедръм',
            5 => 'Вършец',
            6 => 'Георги Дамяново',
            7 => 'Лом',
            8 => 'Медковец',
            9 => 'Монтана',
            10 => 'Чипровци',
            11 => 'Якимово'
        ],
        13 => [
            1 => 'Батак',
            2 => 'Белово',
            3 => 'Брацигово',
            4 => 'Велинград',
            5 => 'Лесичово',
            6 => 'Пазарджик',
            7 => 'Панагюрище',
            8 => 'Пещера',
            9 => 'Ракитово',
            10 => 'Септември',
            11 => 'Стрелча',
            12 => 'Сърница'
        ],
        14 =>[
            1 => 'Брезник',
            2 => 'Земен',
            3 => 'Ковачевци',
            4 => 'Перник',
            5 => 'Радомир',
            6 => 'Трън'
        ],
        15 =>[
            1 =>	 'Белене',
            2 => 'Гулянци',
            3 => 'Долна Митрополия',
            4 => 'Долни Дъбник',
            5 => 'Искър',
            6 => 'Кнежа',
            7 => 'Левски',
            8 => 'Никопол',
            9 => 'Плевен',
            10 => 'Пордим',
            11 => 'Червен бряг'
        ],
        16 => [
            1=> 'Асеновград',
            2=> 'Брезово',
            3=> 'Калояново',
            4=> 'Карлово',
            5=> 'Кричим',
            6=> 'Куклен',
            7=> 'Лъки',
            8=> 'Марица',
            9=> 'Перущица',
            10=> 'Пловдив',
            11=> 'Първомай',
            12=> 'Раковски',
            13=> 'Родопи',
            14=> 'Садово',
            15=> 'Сопот',
            16=> 'Стамболийски',
            17=> 'Съединение',
            18=> 'Хисаря'
        ],
        17 => [
            1 => 'Завет',
            2 => 'Исперих',
            3 => 'Кубрат',
            4 => 'Лозница',
            5 => 'Разград',
            6 => 'Самуил',
            7 => 'Цар Калоян'
        ],
        18 => [
            1 => 'Борово',
            2 => 'Бяла',
            3 => 'Ветово',
            4 => 'Две могили',
            5 => 'Иваново',
            6 => 'Русе',
            7 => 'Сливо поле',
            8 => 'Ценово'
        ],
        19 => [
            1 => 'Алфатар',
            2 => 'Главиница',
            3 => 'Дулово',
            4 => 'Кайнарджа',
            5 => 'Силистра',
            6 => 'Ситово',
            7 => 'Тутракан'
        ],
        20 => [
            1 => 'Котел',
            2 => 'Нова Загора',
            3 => 'Сливен',
            4 => 'Твърдица',
        ],
        21 => [
            1 =>'Баните',
            2 => 'Борино',
            3 => 'Девин',
            4 => 'Доспат',
            5 => 'Златоград',
            6 => 'Мадан',
            7 => 'Неделино',
            8 => 'Рудозем',
            9 => 'Смолян',
            10 => 'Чепеларе'
        ],
        22 => [
            1=> 'Антон',
            2=> 'Божурище',
            3=> 'Ботевград',
            4=> 'Годеч',
            5=> 'Горна Малина',
            6=> 'Долна баня',
            7=> 'Драгоман',
            8=> 'Елин Пелин',
            9=> 'Етрополе',
            10=> 'Златица',
            11=> 'Ихтиман',
            12=> 'Копривщица',
            13=> 'Костенец',
            14=> 'Костинброд',
            15=> 'Мирково',
            16=> 'Пирдоп',
            17=> 'Правец',
            18=> 'Самоков',
            19=> 'Своге',
            20=> 'Сливница',
            21=> 'Чавдар',
            22=> 'Челопеч'
        ],
        23 => [
            1 => 'Братя Даскалови',
            2 => 'Гурково',
            3 => 'Гълъбово',
            4 => 'Казанлък',
            5 => 'Мъглиж',
            6 => 'Николаево',
            7 => 'Опан',
            8 => 'Павел баня',
            9 => 'Раднево',
            10 => 'Стара Загора',
            11 => 'Чирпан'
        ],
        24 => [
            1=>	 'Антоново',
            2=>	 'Омуртаг',
            3=>	 'Опака',
            4=>	 'Попово',
            5=>	 'Търговище'
        ],
        25 => [
            1 => 'Димитровград',
            2 => 'Ивайловград',
            3 => 'Любимец',
            4 => 'Маджарово',
            5 => 'Минерални бани',
            6 => 'Свиленград',
            7 => 'Симеоновград',
            8 => 'Стамболово',
            9 => 'Тополовград',
            10 => 'Харманли',
            11 => 'Хасково'
        ],
        26 => [
            1 => 'Велики Преслав',
            2 => 'Венец',
            3 => 'Върбица',
            4 => 'Каолиново',
            5 => 'Каспичан',
            6 => 'Никола Козлево',
            7 => 'Нови пазар',
            8 => 'Смядово',
            9 => 'Хитрино',
            10 => 'Шумен'
        ],
        27 => [
            1 => 'Болярово',
            2 => 'Елхово',
            3 => 'Стралджа',
            4 => 'Тунджа',
            5 => 'Ямбол'
        ]
    ],

    'locate_municipalities' => [
        ''=> 'Изберете община',
        1 => 'Банско',
        2 => 'Белица',
        3 => 'Благоевград',
        4 => 'Гоце Делчев',
        5 => 'Гърмен',
        6 => 'Кресна',
        7 => 'Петрич',
        8 => 'Разлог',
        9 => 'Сандански',
        10 => 'Сатовча',
        11 => 'Симитли',
        12 => 'Струмяни',
        13 => 'Хаджидимово',
        14 => 'Якоруда',
        15 => 'Оряхово',
        16 => 'Айтос',
        17 => 'Бургас',
        18 => 'Камено',
        19 => 'Карнобат',
        20 => 'Малко Търново',
        21 => 'Несебър',
        22 => 'Поморие',
        23 => 'Приморско',
        24 => 'Руен',
        25 => 'Созопол',
        26 => 'Средец',
        27 => 'Сунгурларе',
        28 => 'Царево',
        29 => 'Каварна',
        30 => 'Аврен',
        31 => 'Аксаково',
        32 => 'Белослав',
        33 => 'Бяла',
        34 => 'Варна',
        35 => 'Ветрино',
        36 => 'Вълчи дол',
        37 => 'Девня',
        38 => 'Долни чифлик',
        39 => 'Дългопол',
        40 => 'Провадия',
        41 => 'Суворово',
        42 => 'Велико Търново',
        43 => 'Горна Оряховица',
        44 => 'Елена',
        45 => 'Златарица',
        46 => 'Лясковец',
        47 => 'Павликени',
        48 => 'Полски Тръмбеш',
        49 => 'Свищов',
        50 => 'Стражица',
        51 => 'Сухиндол',
        52 => 'Априлци',
        53 => 'Белоградчик',
        54 => 'Бойница',
        55 => 'Брегово',
        56 => 'Видин',
        57 => 'Грамада',
        58 => 'Димово',
        59 => 'Кула',
        60 => 'Макреш',
        61 => 'Ново село',
        62 => 'Ружинци',
        63 => 'Чупрене',
        64 => 'Борован',
        65 => 'Бяла Слатина',
        66 => 'Враца',
        67 => 'Козлодуй',
        68 => 'Криводол',
        69 => 'Мездра',
        70 => 'Мизия',
        71 => 'Оряхово',
        72 => 'Роман',
        73 => 'Хайредин',
        74 => 'Габрово',
        75 => 'Дряново',
        76 => 'Севлиево',
        77 => 'Трявна',
        78 => 'Балчик',
        79 => 'Генерал Тошево',
        80 => 'Добрич',
        81 => 'Добрич-селска',
        82 => 'Каварна',
        83 => 'Крушари',
        84 => 'Тервел',
        85 => 'Шабла',
        86 => 'Ардино',
        87 => 'Джебел',
        88 => 'Кирково',
        89 => 'Крумовград',
        90 => 'Кърджали',
        91 => 'Момчилград',
        92 => 'Черноочене',
        93 => 'Бобов дол',
        94 => 'Бобошево',
        95 => 'Дупница',
        96 => 'Кочериново',
        97 => 'Кюстендил',
        98 => 'Невестино',
        99 => 'Рила',
        100 => 'Сапарева баня',
        101 => 'Трекляно',
        102 => 'Априлци',
        103 => 'Летница',
        104 => 'Ловеч',
        105 => 'Луковит',
        106 => 'Тетевен',
        107 => 'Троян',
        108 => 'Угърчин',
        109 => 'Ябланица',
        110 => 'Берковица',
        111 => 'Бойчиновци',
        112 => 'Брусарци',
        113 => 'Вълчедръм',
        114 => 'Вършец',
        115 => 'Георги Дамяново',
        116 => 'Лом',
        117 => 'Медковец',
        118 => 'Монтана',
        119 => 'Чипровци',
        120 => 'Якимово',
        121 => 'Батак',
        122 => 'Белово',
        123 => 'Брацигово',
        124 => 'Велинград',
        125 => 'Лесичово',
        126 => 'Пазарджик',
        127 => 'Панагюрище',
        128 => 'Пещера',
        129 => 'Ракитово',
        130 => 'Септември',
        131 => 'Стрелча',
        132 => 'Сърница',
        133 => 'Брезник',
        134 => 'Земен',
        135 => 'Ковачевци',
        136 => 'Перник',
        137 => 'Радомир',
        138 => 'Трън',
        139 => 'Белене',
        140 => 'Гулянци',
        141 => 'Долна Митрополия',
        142 => 'Долни Дъбник',
        143 => 'Искър',
        144 => 'Кнежа',
        145 => 'Левски',
        146 => 'Никопол',
        147 => 'Плевен',
        148 => 'Пордим',
        149 => 'Червен бряг',
        150 => 'Асеновград',
        151 => 'Брезово',
        152 => 'Калояново',
        153 => 'Карлово',
        154 => 'Кричим',
        155 => 'Куклен',
        156 => 'Лъки',
        157 => 'Марица',
        158 => 'Перущица',
        159 => 'Пловдив',
        160 => 'Първомай',
        161 => 'Раковски',
        162 => 'Родопи',
        163 => 'Садово',
        164 => 'Сопот',
        165 => 'Стамболийски',
        166 => 'Съединение',
        167 => 'Хисаря',
        168 => 'Завет',
        169 => 'Исперих',
        170 => 'Кубрат',
        171 => 'Лозница',
        172 => 'Разград',
        173 => 'Самуил',
        174 => 'Цар Калоян',
        175 => 'Борово',
        176 => 'Бяла',
        177 => 'Ветово',
        178 => 'Две могили',
        179 => 'Иваново',
        180 => 'Русе',
        181 => 'Сливо поле',
        182 => 'Ценово',
        183 => 'Алфатар',
        184 => 'Главиница',
        185 => 'Дулово',
        186 => 'Кайнарджа',
        187 => 'Силистра',
        188 => 'Ситово',
        189 => 'Тутракан',
        190 => 'Котел',
        191 => 'Нова Загора',
        192 => 'Сливен',
        193 => 'Твърдица',
        194 => 'Баните',
        195 => 'Борино',
        196 => 'Девин',
        197 => 'Доспат',
        198 => 'Златоград',
        199 => 'Мадан',
        200 => 'Неделино',
        201 => 'Рудозем',
        202 => 'Смолян',
        203 => 'Чепеларе',
        204 => 'Антон',
        205 => 'Божурище',
        206 => 'Ботевград',
        207 => 'Годеч',
        208 => 'Горна Малина',
        209 => 'Долна баня',
        210 => 'Драгоман',
        211 => 'Елин Пелин',
        212 => 'Етрополе',
        213 => 'Златица',
        214 => 'Ихтиман',
        215 => 'Копривщица',
        216 => 'Костенец',
        217 => 'Костинброд',
        218 => 'Мирково',
        219 => 'Пирдоп',
        220 => 'Правец',
        221 => 'Самоков',
        222 => 'Своге',
        223 => 'Сливница',
        224 => 'Чавдар',
        225 => 'Челопеч',
        226 => 'Братя Даскалови',
        227 => 'Гурково',
        228 => 'Гълъбово',
        229 => 'Казанлък',
        230 => 'Мъглиж',
        231 => 'Николаево',
        232 => 'Опан',
        233 => 'Павел баня',
        234 => 'Раднево',
        235 => 'Стара Загора',
        236 => 'Чирпан',
        237 => 'Антоново',
        238 => 'Омуртаг',
        239 => 'Опака',
        240 => 'Попово',
        241 => 'Търговище',
        242 => 'Димитровград',
        243 => 'Ивайловград',
        244 => 'Любимец',
        245 => 'Маджарово',
        246 => 'Минерални бани',
        247 => 'Свиленград',
        248 => 'Симеоновград',
        249 => 'Стамболово',
        250 => 'Тополовград',
        251 => 'Харманли',
        252 => 'Хасково',
        253 => 'Велики Преслав',
        254 => 'Венец',
        255 => 'Върбица',
        256 => 'Каолиново',
        257 => 'Каспичан',
        258 => 'Никола Козлево',
        259 => 'Нови пазар',
        260 => 'Смядово',
        261 => 'Хитрино',
        262 => 'Шумен',
        263 => 'Болярово',
        264 => 'Елхово',
        265 => 'Стралджа',
        266 => 'Тунджа',
    ],

    'hotel_facilities' => [
        1 => 'СПА център',
        2 => 'Аниматори',
        3 => 'Бар-басейн',
        4 => 'Барбекю',
        5 => 'Баскетбол игрище',
        6 => 'Билярд',
        7 => 'Волейбол игрище',
        8 => 'Паркинг',
        9 => 'Детски басейн',
        10 => 'Детски кът',
        11 => 'Джакузи',
        12 => 'Домашни любимци',
        13 => 'Достъп за инвалиди',
        14 => 'Екскурзовод/водач',
        15 => 'Закрит басейн',
        16 => 'Открит басейн',
        17 => 'Зимна градина',
        18 => 'Изглед',
        19 => 'Wi-Fi',
        20 => 'Казино',
        21 => 'Козметичен салон',
        22 => 'Велосипеди под наем',
        23 => 'Язда',
        24 => 'Конферентна зала',
        25 => 'Лекарски кабинет',
        26 => 'Лоби бар',
        27 => 'Градина',
        28 => 'Механа/кръчма',
        29 => 'Механа за свободно ползване',
        30 => 'Трапезария за свободно ползване',
        31 => 'Ресторант',
        32 => 'Рум сервиз',
        33 => 'Пране',
        34 => 'Плащане с карта',
        35 => 'Нощен бар/клуб',
        36 => 'Автомобили под наем',
        37 => 'Ски гардероб',
        38 => 'Масажи',
        39 => 'Тенис на корт',
        40 => 'Тенис на маса',
        41 => 'Джага',
        42 => 'Фитнес',
        43 => 'Футболно игрище',
        44 => 'Охрана',
        45 => 'Голф',
    ],

    'room_facilities' => [
        1 => 'Спалня',
        2 => 'Единични/о легло',
        3 => 'Хладилник',
        4 => 'Телевизор',
        5 => 'Кабелна/Сателитна телевизия',
        6 => 'Сейф',
        7 => 'Баня/тоалетна',
        8 => 'Пералня',
        9 => 'Печка',
        10 => 'Котлон',
        11 => 'Мини бар',
        12 => 'Джакузи',
        13 => 'Сауна',
        14 => 'Глетка',
        15 => 'Камина',
        16 => 'Микровълнова',
        17 => 'Пералня',
        18 => 'Тераса',
    ],

    'room_types' => [
        '1' => 'Стая',
        '2' => 'Апартамент',
        '3' => 'Студио',
    ],

    'stars' => [
        1 => '1 звезда',
        2 => '2 звезди',
        3 => '3 звезди',
        4 => '4 звезди',
        5 => '5 звезди',
    ],

    'hotel_type' => [
        1 => 'Хотел',
        2 => 'Семеен хотел',
        3 => 'Къща за гости',
        4 => 'Хижа',
        5 => 'Почивна база',
        6 => 'Вилно селище',
        7 => 'Бунгала',
        8 => 'Вила',
    ],

    'visiting_types' => [

        1 => [
            'icon'  => '<i class="soap-icon-businessbag circle"></i>',
            'label' => 'Бизнес'
        ],
        2 => [
            'icon'  => '<i class="soap-icon-couples circle"></i>',
            'label' => 'С половинката'
        ],
        3 => [
            'icon'  => '<i class="soap-icon-family circle"></i>',
            'label' => 'Семейна почивка'
        ],
        4 => [
            'icon'  => '<i class="soap-icon-friends circle"></i>',
            'label' => 'Почивка с приятели'
        ],
        5 => [
            'icon'  => '<i class="soap-icon-user circle"></i>',
            'label' => 'Уединение'
        ],

    ]

];
